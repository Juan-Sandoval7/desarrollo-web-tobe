﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using AppTobe.Models;
using AppTobe.DTOs;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace AppTobe.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuariosController : ControllerBase
    {
        private readonly AppTobeContext _context;
        private readonly IConfiguration configuration;

        public UsuariosController(AppTobeContext context, IConfiguration configuration)
        {
            _context = context;

            this.configuration = configuration;

        }


        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<UsuariosDTO>> GetUsuarios(int id)
        {
            var usuarios = await _context.Usuarios.FindAsync(id);

            if (usuarios == null)
            {
                return NotFound();
            }

            return Mapper.Map<UsuariosDTO>(usuarios);
        }


        [HttpGet]
        [Authorize]
        public IEnumerable<UsuariosDTO> GetCustomer()
        {
            return Mapper.Map<IEnumerable<UsuariosDTO>>(_context.Usuarios.OrderBy(x => x.Nombre));
        }


        [HttpPost("ConsultarUsuarioCorreo")]
        [Authorize]
        public async Task<IActionResult> ConsultarUsuarioCorreo([FromBody] dynamic credentials)
        {
            var username = (string)credentials["username"];

            var customer = await _context.Usuarios.SingleOrDefaultAsync(m => m.Nombredeusuario == username);

            if (customer == null)
            {
                return NotFound();
            }
            return Ok(Mapper.Map<UsuariosDTO>(customer));
        }

        // GET: api/Usuarios/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<UsuariosDTO>> GetUsuarioss(int id)
        {
            var usuarios = await _context.Usuarios.FindAsync(id);

            if (usuarios == null)
            {
                return NotFound();
            }

            return Mapper.Map<UsuariosDTO>(usuarios);
        }


        private string GenerarTokenJWT(UsuariosDTO usuarioInfo)
        {
            // CREAMOS EL HEADER //
            var _symmetricSecurityKey = new SymmetricSecurityKey(
                    Encoding.UTF8.GetBytes(configuration["JWT:ClaveSecreta"])
                );
            var _signingCredentials = new SigningCredentials(
                    _symmetricSecurityKey, SecurityAlgorithms.HmacSha256
                );
            var _Header = new JwtHeader(_signingCredentials);

            // CREAMOS LOS CLAIMS //
            var _Claims = new[] {
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(JwtRegisteredClaimNames.NameId, usuarioInfo.Id.ToString()),
                new Claim("nombre", usuarioInfo.Nombre),
                new Claim("usuario", usuarioInfo.Nombredeusuario),
               // new Claim(ClaimTypes.Role, usuarioInfo.Contraseña)
            };

            // CREAMOS EL PAYLOAD //
            var _Payload = new JwtPayload(
                    issuer: configuration["JWT:Issuer"],
                    audience: configuration["JWT:Audience"],
                    claims: _Claims,
                    notBefore: DateTime.UtcNow,
                    // Exipra a la 24 horas.
                    expires: DateTime.UtcNow.AddHours(24)
                );

            // GENERAMOS EL TOKEN //
            var _Token = new JwtSecurityToken(
                    _Header,
                    _Payload
                );

            return new JwtSecurityTokenHandler().WriteToken(_Token);
        }

       // [HttpGet("ConfirmacionCorreo/{id}")]
      //  public async Task<String> ConfirmacionCorreo(int id)
      //  {
      //      var usuarios = await _context.Usuarios.FindAsync(id);

        //    if (usuarios == null)
        //    {
             //   return "Usuario no encontrado";
        //    }
        //    else
         //   {
                // string query = "SELECT * FROM Usuarios WHERE ID = {0}";
         //       usuarios.Permiso = 1;
          //      _context.SaveChanges();

            //    return "Correo confirmado";
         //   }

       // }

        // PUT: api/Usuarios/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        [Authorize]
        public async Task<IActionResult> PutUsuarios(int id, UsuariosDTO usuarios)
        {
            if (id != usuarios.Id)
            {
                return BadRequest();
            }

            _context.Entry(Mapper.Map<UsuariosDTO>(usuarios)).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!UsuariosExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }


       // [HttpPost("UsuariosporEntidad")]
       // [Authorize]
      //  public IEnumerable<UsuariosDTO> UsuariosporEntidad([FromBody] dynamic credentials)
      //  {
      //      var idEntidad = (int)credentials["identidad"];
      //      var Usuarios = _context.Usuarios.OrderBy(x => x.Nombre).Where(x => x.Identidad == idEntidad).ToList();

       //     return Mapper.Map<IEnumerable<UsuariosDTO>>(Usuarios);
       // }


        [HttpPost("Login")]
        public async Task<IActionResult> Login([FromBody] dynamic credentials)
        {
            var username = (string)credentials["username"];
            var password = (string)credentials["password"];

            var customer = await _context.Usuarios.SingleOrDefaultAsync(m => m.Nombredeusuario == username && m.Contraseña == password);

            if (customer == null)
            {
                return NotFound();
            }
            return Ok(Mapper.Map<UsuariosDTO>(customer));
        }


        [HttpPost("LoginToken")]
        [AllowAnonymous]
        public async Task<IActionResult> LoginToken([FromBody] dynamic credentials)
        {
            var username = (string)credentials["username"];
            var password = (string)credentials["password"];

            var _user = await _context.Usuarios.SingleOrDefaultAsync(m => m.Nombredeusuario == username && m.Contraseña == password);

            if (_user == null)
            {
                return Unauthorized(); 
            }
            else
            {
                return Ok(new { token = GenerarTokenJWT(Mapper.Map<UsuariosDTO>(_user)) });

            }
        }

        [HttpPost("ConsultarUsuario")]
        public async Task<bool> ConsultarUsuario([FromBody] dynamic credentials)
        {
            var username = (string)credentials["correo"];
            
            var customer = await _context.Usuarios.SingleOrDefaultAsync(m => m.Nombredeusuario == username);

            if (customer == null)
            {
                return false;
            }

            return true;
        }


        // POST: api/Usuarios
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
      //  [Authorize]
        public async Task<ActionResult<Usuarios>> PostUsuarios(UsuariosDTO usuarios)
        {
            var map = Mapper.Map<Usuarios>(usuarios);
            _context.Usuarios.Add(map);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUsuarios", new { id = usuarios.Id }, usuarios);
        }

        // DELETE: api/Usuarios/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<ActionResult<UsuariosDTO>> DeleteUsuarios(int id)
        {
            var usuarios = await _context.Usuarios.FindAsync(id);
            if (usuarios == null)
            {
                return NotFound();
            }

            _context.Usuarios.Remove(usuarios);
            await _context.SaveChangesAsync();

            return Mapper.Map<UsuariosDTO>(usuarios);
        }

        private bool UsuariosExists(int id)
        {
            return _context.Usuarios.Any(e => e.Id == id);
        }
    }
}
