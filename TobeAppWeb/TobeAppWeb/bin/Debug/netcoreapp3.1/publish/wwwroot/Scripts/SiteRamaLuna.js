﻿$(document).ready(function () {
    // select_padre
    function getMobileOperatingSystem() {
        var userAgent = navigator.userAgent || navigator.vendor || window.opera;

        // Windows Phone must come first because its UA also contains "Android"
        if (/windows phone/i.test(userAgent)) {
        }

        if (/android/i.test(userAgent)) {
        }

        // iOS detection from: http://stackoverflow.com/a/9039885/177710
        if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
            $(".flechasApple").attr("style", " ");
        }

        return "unknown";
    }
    

    getMobileOperatingSystem();
    $(':input[type="number"]').attr("min", 0);

    $("html").find("iframe").each(function () {
        if ($(this).contents().find("#isLogin").val() == 1) {
            window.parent.location.href = "/";
        }
    });

    $("iframe").load(function () {
        if ($(this).contents().find("#isLogin").val() == 1) {
            window.parent.location.href = "/";
        }
    });

    $("#tipo_menu").change(function () {
        if ($(this).val() == 1) {
            $("#select_padre").hide();
            $(".titmenpadre").hide();
            $("#select_padre").val(null);
        } else {
            $("#select_padre").show();
            $(".titmenpadre").show();

        }
    })
    $(".mayuscula").keyup(function () {
        $(this).val($(this).val().toUpperCase());
    })
    $('.disabled').attr("href", "#");

    $('.bloquearClick').click(function () {
        $(this).prop('disabled', true);
    });


    /* ++++++++++++++ funciones para el menu principal */
    var id_menu;
    $("#iframeChat").hide();
    /*$(".contenedor-padre-menu").css("width", $(window).width());*/
    $("#mostrarChat").click(function () {
        $("#iframeChat").slideDown();
    });
    $(".item").click(function () {

        if ($("#padre_" + $(this).attr("id")).children().length == 1 || $("#padre_" + $(this).attr("id")).html() == undefined) {
            if ($(this).attr("url") != undefined) {
                window.location.href = "../" + $(this).attr("url");
            }
        }
    });

    $('.menu-item').click(function () {

        if ($("#padre_" + $(this).attr("id")).children().html() != undefined && $("#padre_" + $(this).attr("id")).children().length > 1) {
            id_menu = $(this).attr("id");

            $("#menu_titulo_modal").text($(this).attr("nombre"));

            jQuery.fancybox.open({ href: "#padre_" + id_menu, autoDimensions: true });
        }

    });


    var q = $(".menu-hijo");

    for (var l = 0; l < q.length; l++) {
        $(q[l]).appendTo("#padre_" + $(q[l]).attr("id_padre"));
        $("#padre_" + $(q[l]).attr("id_padre")).parent().attr("url", ($(q[l]).attr("url")));
        $(q[l]).show();
    }

    /* ++++++++++++++ fin funciones para el menu principal */

    $('[data-toggle="tooltip"]').tooltip();

    $(".text-menu").show();

    //$("#sidebar-wrapper").css("left", "120px");

    // $(".text-menu").hide();
    $(".icon_menu").parent().css("text-align", "right");

    /* ++++++++++++++++++++++++++++++++++++++++++++++++++++ MENU DERECHA +++++++++++++++++++++++ */

    $('#iframeBaseHistoriaClinica').iframeTracker({
        blurCallback: function () {
            if ($("body").width() < 1024) {
                var width2 = -200; $("#boton-menu-i").show();
                $("#sidebar-wrapper").animate({ "right": width2 + "px" }, "fast");
                $("#boton-menu-i").show();

                width2 = 0;

                if ($("#IdMenuBase").val() == 1) {
                    $("#boton-menu-historia").show();
                }

                $(".menu-derecho").animate({ "left": width2 + "px" }, "fast");
                banderaMenu = 0;
            }
        }
    });

    $("#sidebar-wrapper").hover(
      function () {
          $(this).animate({ "right": "0px" }, "fast", function () {
              // Animation complete.

          });

      }, function () {
          $(this).animate({ "right": "-124px" }, "fast", function () {
              // Animation complete.
          });

      }
    );

    $("#boton-menu-i").hover(
      function () {
          $(this).animate({ "right": "0px" }, "fast", function () {
              // Animation complete.

          });

      }, function () {
          $(this).animate({ "right": "-124px" }, "fast", function () {
              // Animation complete.
          });

      }
    );
    if ($("body").width() < 877) {

        $("#sidebar-wrapper").css("right", "-200px");
        $(".menu-derecho").css("left", "0px");
        $("#iframeMenu").css("height", "218px");
        $("#iframeMenu").attr("scrolling", "yes");

        if ($("#IdMenuBase").val() == 1) {
            $("#boton-menu-historia").show();
        }
    } else {
        $("#boton-menu-historia").hide();
    }

    var container = $("#boton-menu-i");



    var mouse_is_inside = false;

    $('#boton-menu-i').hover(function () {
        mouse_is_inside = true;
    }, function () {
        mouse_is_inside = false;
    });

    //$(document).on("click", function () {
    //    alert("test");
    //    $(document).t
    //});

    $("p").on("tap", function () {
        $(this).hide();
    });

    $("body").mouseup(function () {

        if (mouse_is_inside) {
            $("#sidebar-wrapper").animate({ "right": "0px" }, "fast", function () {
                $("#boton-menu-i").hide();
            });
        } else {
            if ($("body").width() < 877) { var width2 = -200; $("#boton-menu-i").show(); } else {
                var width2 = -124;
            }
            $("#sidebar-wrapper").animate({ "right": width2 + "px" }, "fast", function () {
                // Animation complete.
            });
        }
    });


    /* ++++++++++++++++++++++++++++++++++++++++++++ ACCION BOTON MENU MOVIL HISTORIA CLINICA +++*/
    var mouse_is_inside_historia = false;

    $('#boton-menu-historia').hover(function () {
        mouse_is_inside_historia = true;
    }, function () {
        mouse_is_inside_historia = false;
    });

    $("body").mouseup(function () {

        if (mouse_is_inside_historia) {
            $(".menu-derecho").animate({ "left": "250px" }, "fast", function () {
                // Animation complete.
                $("#boton-menu-historia").hide();

            });
            $("#iframeMenu").animate({ "opacity": "1" }, "fast");
        } else {
            if ($("body").width() < 877) {
                var width2 = 0;

                if ($("#IdMenuBase").val() == 1) {
                    $("#boton-menu-historia").show();
                }
            } else {
                var width2 = 100;
            }
            $(".menu-derecho").animate({ "left": width2 + "px" }, "fast", function () {

            });
        }
    });

    /* ++++++++++++++++++++++ FIN ACCION BOTON MENU MOVIL HISTORIA CLINICA +++++++++++++++++++++++++++*/

    /////Código para ventana modal de Fancy Box
    if (jQuery().fancybox) {
        $("#VentanasModalesFancyBox").fancybox({
            type: 'iframe',
            scrolling: 'auto',
            closeClick: true,
            autoDimensions: true,
            transitionIn: 'fade',
            transitionOut: 'fade',
            padding: 1,
            preload: true,
            afterClose: function () {
                RefrescarPadre();
            }
        });
    }
    if (jQuery().fancybox) {
        $("#VentanasModalesFancyBoxSinRefrescarPadre").fancybox({
            type: 'iframe',
            scrolling: 'auto',
            closeClick: true,
            autoDimensions: true,
            transitionIn: 'fade',
            transitionOut: 'fade',
            padding: 1,
            preload: true,
        });
    }

    if (jQuery().fancybox) {
        $("#VentanasModalesFancyBoxSinRefrescarPadreChat").fancybox({
            type: 'iframe',
            scrolling: 'auto',
            closeClick: true,
            autoDimensions: true,
            transitionIn: 'fade',
            transitionOut: 'fade',
            padding: 1,
            preload: true,
        });
    }
    /////Fin: Código para ventana modal de Fancy Box




});

/////Código para ventana modal de Fancy Box
function RefrescarPadre() {
    setTimeout(function () {
        location.reload();
    }, 2000);
}

function RecargarMenu() {

    var src = window.parent.$("#iframeMenu").attr("src");

    setTimeout(function () {
        window.parent.$("#iframeMenu").attr("src", src);
    }, 3000);
}

function MostrarModalFancyBox(ruta) {
    $("#VentanasModalesFancyBox").attr("href", "https://" + window.location.host + "/" + ruta);
    $("#VentanasModalesFancyBox").click();
}
function MostrarModalFancyBoxSinRefrescarPadre(ruta) {
    $("#VentanasModalesFancyBoxSinRefrescarPadre").attr("href", "https://" + window.location.host + "/" + ruta);
    $("#VentanasModalesFancyBoxSinRefrescarPadre").click();
}
/////Fin: Código para ventana modal de Fancy Box
function cerrarfancybox() {
    parent.jQuery.fancybox.close();
}
var basedeURL;

////crea la base de url en javascript de la aplicacion
var partesUrl = window.location.pathname.split('/');
var prefijoAplicacion = "";
if (partesUrl[1] && partesUrl[1].length > 0) {
    prefijoAplicacion = "/" + partesUrl[1];
}
basedeURL = "https://" + window.location.host;



////crea la base de url en javascript de la aplicacion
