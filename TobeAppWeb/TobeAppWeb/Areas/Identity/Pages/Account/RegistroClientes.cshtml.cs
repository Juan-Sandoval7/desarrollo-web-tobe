using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using TobeAppWeb.Models;

namespace TobeAppWeb.Areas.Identity.Pages.Account
{
    [AllowAnonymous]
    public class RegistroClientesModel : PageModel
    {

        private readonly AppTobeContext _context;

        public RegistroClientesModel(AppTobeContext context)
        {
            _context = context;
        }
        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        //  public IList<AuthenticationScheme> ExternalLogins { get; set; }

        public class InputModel
        {

            public string Nombre { get; set; }
            [Required(ErrorMessage = "El nombre de usuario")]
            public string Nombredeusuario { get; set; }
            [Required(ErrorMessage = "La contrase�a es obligatorio")]
            [DataType(DataType.Password)]
            public string Contrase�a { get; set; }
            [Required(ErrorMessage = "El correo es obligatorio")]
            public string Correo { get; set; }
            [Required(ErrorMessage = "El telefono es obligatorio")]
            public string Telefono { get; set; }
            [Required(ErrorMessage = "El lugar de nacimiento es obligatorio")]
            public string Lugardenacimiento { get; set; }
            [Required(ErrorMessage = "La fecha de nacimiento es obligatorio")]
            public string Fechadenacimiento { get; set; }
            [Required(ErrorMessage = "El genero es obligatorio")]
            public string Genero { get; set; }
            [Required(ErrorMessage = "La formaci�n es obligatorio")]
            public string Formacion { get; set; }


        }

        public async Task OnGetAsync(string returnUrl = null)
        {
            List<string> tipo = new List<string>();
            tipo.Add("No tiene formaci�n");
            tipo.Add("Bachiller");
            tipo.Add("Profesional");
            tipo.Add("Magister");
            tipo.Add("Doctorado");
            tipo.Add("Posgrado");
            ViewData["Formacion"] = new SelectList(tipo);

            List<string> genero = new List<string>();
            genero.Add("Femenino");
            genero.Add("Masculino");
            ViewData["genero"] = new SelectList(genero);
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            if (ModelState.IsValid)
            {
                var user = new Usuarios
                {
                    Nombre = Input.Nombre,
                    Nombredeusuario = Input.Nombredeusuario,
                    Contrase�a = Input.Contrase�a,
                    Correo = Input.Correo,
                    Telefono = Input.Telefono,
                    Lugardenacimiento = Input.Lugardenacimiento,
                    Fechadenacimiento = Input.Fechadenacimiento,
                    Genero = Input.Genero,
                    Formacion = Input.Formacion,
                };
                try
                {

                    _context.Usuarios.Add(user);
                    await _context.SaveChangesAsync();
                    ModelState.AddModelError(string.Empty, "Usuario registrado");
                    return RedirectToPage("./Login");
                }
                catch (Exception e)
                {
                    ModelState.AddModelError(string.Empty, "el nombre de usuario o correo ya estan registrados");
                    // return RedirectToAction("Index", "asp_Net_Users");
                }
            }
            // return RedirectToAction(nameof(Index));

            // return View(usuarios);
            // If we got this far, something failed, redisplay form
            return Page();
        }
    }
}
