﻿using System;
using System.Collections.Generic;

namespace TobeAppWeb.Models
{
    public partial class PermisoUsuarios
    {
        public int Id { get; set; }
        public int? IdUsuarios { get; set; }
        public int? IdEntidad { get; set; }

        public virtual Entidades IdEntidadNavigation { get; set; }
        public virtual Usuarios IdUsuariosNavigation { get; set; }
    }
}
