﻿using System;
using System.Collections.Generic;

namespace TobeAppWeb.Models
{
    public partial class Items
    {
        public Items()
        {
            Calificaciones = new HashSet<Calificaciones>();
        }

        public int Id { get; set; }
        public int? IdSubCategoria { get; set; }
        public string NombreItem { get; set; }
        public string Descripcion { get; set; }

        public virtual Subcategorias IdSubCategoriaNavigation { get; set; }
        public virtual ICollection<Calificaciones> Calificaciones { get; set; }
    }
}
