﻿using System;
using System.Collections.Generic;

namespace TobeAppWeb.Models
{
    public partial class Calificaciones
    {
        public int Id { get; set; }
        public int IdUsuario { get; set; }
        public int IdItems { get; set; }
        public int IdSede { get; set; }
        public DateTime? Fecha { get; set; }
        public double Valoracion { get; set; }
        public string Comentario { get; set; }

        public virtual Items IdItemsNavigation { get; set; }
        public virtual Sedes IdSedeNavigation { get; set; }
        public virtual Usuarios IdUsuarioNavigation { get; set; }
    }
}
