﻿using System;
using System.Collections.Generic;

namespace TobeAppWeb.Models
{
    public partial class NotificacionesaReporteCovid
    {
        public int Id { get; set; }
        public int? IdReporteCovid { get; set; }
        public string EntidadNotifica { get; set; }
        public DateTime? FechaNotifica { get; set; }
        public string IraTrabajar { get; set; }
        public string CitaTelemedicina { get; set; }

        public virtual ReporteCovid IdReporteCov { get; set; }
    }
}
