﻿using System;
using System.Collections.Generic;

namespace TobeAppWeb.Models
{
    public partial class Entidades
    {
        public Entidades()
        {
            AspNetUsers = new HashSet<AspNetUsers>();
            PermisoUsuarios = new HashSet<PermisoUsuarios>();
            Sedes = new HashSet<Sedes>();
        }

        public int Id { get; set; }
        public string Entidad { get; set; }
        public string Descripcion { get; set; }
        public string Logo { get; set; }
        public string Color { get; set; }
        public string LinkReportes { get; set; }

        public virtual ICollection<AspNetUsers> AspNetUsers { get; set; }
        public virtual ICollection<PermisoUsuarios> PermisoUsuarios { get; set; }
        public virtual ICollection<Sedes> Sedes { get; set; }
    }
}
