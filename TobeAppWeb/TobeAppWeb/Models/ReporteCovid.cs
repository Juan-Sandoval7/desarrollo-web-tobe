﻿using System;
using System.Collections.Generic;

namespace TobeAppWeb.Models
{
    public partial class ReporteCovid
    {
        public ReporteCovid()
        {
            NotificacionesaReporteCovid = new HashSet<NotificacionesaReporteCovid>();
        }

        public int Id { get; set; }
        public int IdUsuario { get; set; }
        public int? IdSedes { get; set; }
        public string Nombre { get; set; }
        public string NumerodeIdentificacion { get; set; }
        public DateTime? Fecha { get; set; }
        public string AreadeTrabajo { get; set; }
        public string Sintomas { get; set; }
        public string SintomasFamilia { get; set; }
        public string Rta6 { get; set; }
        public string Rta7 { get; set; }
        public string Rta8 { get; set; }
        public string Rta9 { get; set; }
        public string Estadodeanimo { get; set; }
        public string Enfermedades { get; set; }
        public string Iratrabajar { get; set; }
        public string Comentarios { get; set; }
        public int NumerodeSintomas { get; set; }
        public int Notificado { get; set; }
        public bool? NotificadoU { get; set; }

        public virtual Sedes IdSedesNavigation { get; set; }
        public virtual Usuarios IdUsuarioNavigation { get; set; }
        public virtual ICollection<NotificacionesaReporteCovid> NotificacionesaReporteCovid { get; set; }
    }
}
