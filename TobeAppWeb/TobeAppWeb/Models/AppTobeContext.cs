﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace TobeAppWeb.Models
{
    public partial class AppTobeContext : DbContext
    {
        public AppTobeContext()
        {
        }

        public AppTobeContext(DbContextOptions<AppTobeContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AspNetRoleClaims> AspNetRoleClaims { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        public virtual DbSet<AspNetUserTokens> AspNetUserTokens { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<Calificaciones> Calificaciones { get; set; }
        public virtual DbSet<Entidades> Entidades { get; set; }
        public virtual DbSet<EventosyNoticias> EventosyNoticias { get; set; }
        public virtual DbSet<Items> Items { get; set; }
        public virtual DbSet<NotificacionesaReporteCovid> NotificacionesaReporteCovid { get; set; }
        public virtual DbSet<PermisoUsuarios> PermisoUsuarios { get; set; }
        public virtual DbSet<ReporteCovid> ReporteCovid { get; set; }
        public virtual DbSet<Sedes> Sedes { get; set; }
        public virtual DbSet<Subcategorias> Subcategorias { get; set; }
        public virtual DbSet<Usuarios> Usuarios { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseSqlServer("Server=tobedb.csgjdxae2i2f.us-east-2.rds.amazonaws.com;Database=AppTobe;User Id=admin;Password=Giseller93");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AspNetRoleClaims>(entity =>
            {
                entity.HasIndex(e => e.RoleId);

                entity.Property(e => e.RoleId).IsRequired();

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetRoleClaims)
                    .HasForeignKey(d => d.RoleId);
            });

            modelBuilder.Entity<AspNetRoles>(entity =>
            {
                entity.HasIndex(e => e.NormalizedName)
                    .HasName("RoleNameIndex")
                    .IsUnique()
                    .HasFilter("([NormalizedName] IS NOT NULL)");

                entity.Property(e => e.Name).HasMaxLength(256);

                entity.Property(e => e.NormalizedName).HasMaxLength(256);
            });

            modelBuilder.Entity<AspNetUserClaims>(entity =>
            {
                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserClaims)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserLogins>(entity =>
            {
                entity.HasKey(e => new { e.LoginProvider, e.ProviderKey });

                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.LoginProvider).HasMaxLength(128);

                entity.Property(e => e.ProviderKey).HasMaxLength(128);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserLogins)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserRoles>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId });

                entity.HasIndex(e => e.RoleId);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.RoleId);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserTokens>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.LoginProvider, e.Name });

                entity.Property(e => e.LoginProvider).HasMaxLength(128);

                entity.Property(e => e.Name).HasMaxLength(128);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserTokens)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUsers>(entity =>
            {
                entity.HasIndex(e => e.NormalizedEmail)
                    .HasName("EmailIndex");

                entity.HasIndex(e => e.NormalizedUserName)
                    .HasName("UserNameIndex")
                    .IsUnique()
                    .HasFilter("([NormalizedUserName] IS NOT NULL)");

                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.NormalizedEmail).HasMaxLength(256);

                entity.Property(e => e.NormalizedUserName).HasMaxLength(256);

                entity.Property(e => e.UserName).HasMaxLength(256);

                entity.HasOne(d => d.IdEntidadNavigation)
                    .WithMany(p => p.AspNetUsers)
                    .HasForeignKey(d => d.IdEntidad)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_AspNetUsers_Entidades");
            });

            modelBuilder.Entity<Calificaciones>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Comentario)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Fecha).HasColumnType("date");

                entity.Property(e => e.IdItems).HasColumnName("ID_Items");

                entity.Property(e => e.IdSede).HasColumnName("ID_Sede");

                entity.Property(e => e.IdUsuario).HasColumnName("ID_usuario");

                entity.Property(e => e.Valoracion).HasColumnName("valoracion");

                entity.HasOne(d => d.IdItemsNavigation)
                    .WithMany(p => p.Calificaciones)
                    .HasForeignKey(d => d.IdItems)
                    .HasConstraintName("FK_Calificaciones_Items");

                entity.HasOne(d => d.IdSedeNavigation)
                    .WithMany(p => p.Calificaciones)
                    .HasForeignKey(d => d.IdSede)
                    .HasConstraintName("FK_Calificaciones_Sedes");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.Calificaciones)
                    .HasForeignKey(d => d.IdUsuario)
                    .HasConstraintName("FK_Calificaciones_usuario");
            });

            modelBuilder.Entity<Entidades>(entity =>
            {
                entity.HasIndex(e => e.Entidad)
                    .HasName("IX_Entidades")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Color)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Descripcion)
                    .IsRequired()
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Entidad)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.LinkReportes)
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Logo)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<EventosyNoticias>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(300)
                    .IsUnicode(false);

                entity.Property(e => e.IdSede).HasColumnName("ID_Sede");

                entity.Property(e => e.Imagen)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Titulo)
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdSedeNavigation)
                    .WithMany(p => p.EventosyNoticias)
                    .HasForeignKey(d => d.IdSede)
                    .HasConstraintName("FK_Sedes_EventosyNoticias");
            });

            modelBuilder.Entity<Items>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Descripcion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.IdSubCategoria).HasColumnName("ID_SubCategoria");

                entity.Property(e => e.NombreItem)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdSubCategoriaNavigation)
                    .WithMany(p => p.Items)
                    .HasForeignKey(d => d.IdSubCategoria)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Items_Subcategorías");
            });

            modelBuilder.Entity<NotificacionesaReporteCovid>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.CitaTelemedicina)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.EntidadNotifica)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.FechaNotifica).HasColumnType("date");

                entity.Property(e => e.IraTrabajar)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdReporteCov)
                    .WithMany(p => p.NotificacionesaReporteCovid)
                    .HasForeignKey(d => d.IdReporteCovid)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_NotificacionesaReporteCovid_ReporteCovid");
            });

            modelBuilder.Entity<PermisoUsuarios>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.IdEntidad).HasColumnName("idEntidad");

                entity.HasOne(d => d.IdEntidadNavigation)
                    .WithMany(p => p.PermisoUsuarios)
                    .HasForeignKey(d => d.IdEntidad)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_PermisoUsuarios_Entidades");

                entity.HasOne(d => d.IdUsuariosNavigation)
                    .WithMany(p => p.PermisoUsuarios)
                    .HasForeignKey(d => d.IdUsuarios)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_PermisoUsuarios_Usuarios");
            });

            modelBuilder.Entity<ReporteCovid>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.AreadeTrabajo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Comentarios)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Enfermedades)
                    .IsRequired()
                    .HasMaxLength(800)
                    .IsUnicode(false);

                entity.Property(e => e.Estadodeanimo)
                    .HasMaxLength(800)
                    .IsUnicode(false);

                entity.Property(e => e.Fecha).HasColumnType("date");

                entity.Property(e => e.Iratrabajar)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.NumerodeIdentificacion)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Rta6)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Rta7)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Rta8)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Rta9)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Sintomas)
                    .IsRequired()
                    .HasMaxLength(800)
                    .IsUnicode(false);

                entity.Property(e => e.SintomasFamilia)
                    .IsRequired()
                    .HasMaxLength(800)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdSedesNavigation)
                    .WithMany(p => p.ReporteCovid)
                    .HasForeignKey(d => d.IdSedes)
                    .HasConstraintName("FK_ReporteCovid_Sedes");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.ReporteCovid)
                    .HasForeignKey(d => d.IdUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ReporteCovid_Usuarios");
            });

            modelBuilder.Entity<Sedes>(entity =>
            {
                entity.HasIndex(e => e.NombreSede)
                    .HasName("IX_Sedes")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Ciudad)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Departamento)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.Property(e => e.Direccion)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.IdEntidad).HasColumnName("ID_ENTIDAD");

                entity.Property(e => e.NombreSede)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Pais)
                    .IsRequired()
                    .HasMaxLength(30)
                    .IsUnicode(false);

                entity.HasOne(d => d.IdEntidadNavigation)
                    .WithMany(p => p.Sedes)
                    .HasForeignKey(d => d.IdEntidad)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("FK_Entidades_Sedes");
            });

            modelBuilder.Entity<Subcategorias>(entity =>
            {
                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Descripción)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Usuarios>(entity =>
            {
                entity.HasIndex(e => e.Correo)
                    .HasName("IX_Usuarios_1")
                    .IsUnique();

                entity.HasIndex(e => e.Nombredeusuario)
                    .HasName("IX_Usuarios")
                    .IsUnique();

                entity.Property(e => e.Id).HasColumnName("ID");

                entity.Property(e => e.Contraseña)
                    .IsRequired()
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.Correo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Fechadenacimiento)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Formacion)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Genero)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Lugardenacimiento)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nombre)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Nombredeusuario)
                    .IsRequired()
                    .HasMaxLength(32)
                    .IsUnicode(false);

                entity.Property(e => e.Telefono)
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
