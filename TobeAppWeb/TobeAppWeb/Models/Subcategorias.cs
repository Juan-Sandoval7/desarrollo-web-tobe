﻿using System;
using System.Collections.Generic;

namespace TobeAppWeb.Models
{
    public partial class Subcategorias
    {
        public Subcategorias()
        {
            Items = new HashSet<Items>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Descripción { get; set; }

        public virtual ICollection<Items> Items { get; set; }
    }
}
