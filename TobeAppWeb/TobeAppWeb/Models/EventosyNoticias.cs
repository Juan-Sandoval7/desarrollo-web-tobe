﻿using System;
using System.Collections.Generic;

namespace TobeAppWeb.Models
{
    public partial class EventosyNoticias
    {
        public int Id { get; set; }
        public int IdSede { get; set; }
        public int Tipo { get; set; }
        public string Titulo { get; set; }
        public string Descripcion { get; set; }
        public string Imagen { get; set; }

        public virtual Sedes IdSedeNavigation { get; set; }
    }
}
