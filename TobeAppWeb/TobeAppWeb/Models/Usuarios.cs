﻿using System;
using System.Collections.Generic;

namespace TobeAppWeb.Models
{
    public partial class Usuarios
    {
        public Usuarios()
        {
            Calificaciones = new HashSet<Calificaciones>();
            PermisoUsuarios = new HashSet<PermisoUsuarios>();
            ReporteCovid = new HashSet<ReporteCovid>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Nombredeusuario { get; set; }
        public string Contraseña { get; set; }
        public string Correo { get; set; }
        public string Telefono { get; set; }
        public string Lugardenacimiento { get; set; }
        public string Fechadenacimiento { get; set; }
        public string Genero { get; set; }
        public string Formacion { get; set; }

        public virtual ICollection<Calificaciones> Calificaciones { get; set; }
        public virtual ICollection<PermisoUsuarios> PermisoUsuarios { get; set; }
        public virtual ICollection<ReporteCovid> ReporteCovid { get; set; }
    }
}
