﻿using System;
using System.Collections.Generic;

namespace TobeAppWeb.Models
{
    public partial class Sedes
    {
        public Sedes()
        {
            Calificaciones = new HashSet<Calificaciones>();
            EventosyNoticias = new HashSet<EventosyNoticias>();
            ReporteCovid = new HashSet<ReporteCovid>();
        }

        public int Id { get; set; }
        public int? IdEntidad { get; set; }
        public string NombreSede { get; set; }
        public string Pais { get; set; }
        public string Departamento { get; set; }
        public string Ciudad { get; set; }
        public string Direccion { get; set; }
        public double Latitud { get; set; }
        public double Longitud { get; set; }

        public virtual Entidades IdEntidadNavigation { get; set; }
        public virtual ICollection<Calificaciones> Calificaciones { get; set; }
        public virtual ICollection<EventosyNoticias> EventosyNoticias { get; set; }
        public virtual ICollection<ReporteCovid> ReporteCovid { get; set; }
    }
}
