﻿using System;
using System.Collections.Generic;

namespace TobeAppWeb.Models
{
    public partial class Datosusuarios
    {
        public int Id { get; set; }
        public int? IdUsuario { get; set; }
        public string Correo { get; set; }
        public string Telefono { get; set; }
        public string Lugardenacimiento { get; set; }
        public string Fechadenacimiento { get; set; }
        public string Genero { get; set; }
        public string Formacion { get; set; }

        public virtual Usuarios IdUsuarioNavigation { get; set; }
    }
}
