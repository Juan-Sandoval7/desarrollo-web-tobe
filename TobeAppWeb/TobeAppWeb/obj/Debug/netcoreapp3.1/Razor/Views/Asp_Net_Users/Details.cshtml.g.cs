#pragma checksum "C:\Users\ANA ROJAS\Source\Repos\desarrollo-web-tobe\TobeAppWeb\TobeAppWeb\Views\Asp_Net_Users\Details.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "2408d07b6b41e46a23c681e97118059e3db383df"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Asp_Net_Users_Details), @"mvc.1.0.view", @"/Views/Asp_Net_Users/Details.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\ANA ROJAS\Source\Repos\desarrollo-web-tobe\TobeAppWeb\TobeAppWeb\Views\_ViewImports.cshtml"
using TobeAppWeb;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\ANA ROJAS\Source\Repos\desarrollo-web-tobe\TobeAppWeb\TobeAppWeb\Views\_ViewImports.cshtml"
using TobeAppWeb.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"2408d07b6b41e46a23c681e97118059e3db383df", @"/Views/Asp_Net_Users/Details.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"823eb05a959383dc96a57ee35f893c0831292d6b", @"/Views/_ViewImports.cshtml")]
    public class Views_Asp_Net_Users_Details : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<TobeAppWeb.Models.AspNetUsers>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Edit", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("asp-action", "Index", global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper;
        private global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral("\r\n");
#nullable restore
#line 3 "C:\Users\ANA ROJAS\Source\Repos\desarrollo-web-tobe\TobeAppWeb\TobeAppWeb\Views\Asp_Net_Users\Details.cshtml"
  
    ViewData["Title"] = "Details";
    Layout = "~/Views/Shared/_LayoutTobe.cshtml";

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("head", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "2408d07b6b41e46a23c681e97118059e3db383df4252", async() => {
                WriteLiteral("\r\n    <link rel=\"stylesheet\" href=\"/css/Detalles.css\" />\r\n    <meta name=\"viewport\" content=\"width=device-width\" />\r\n    <title>");
#nullable restore
#line 11 "C:\Users\ANA ROJAS\Source\Repos\desarrollo-web-tobe\TobeAppWeb\TobeAppWeb\Views\Asp_Net_Users\Details.cshtml"
      Write(ViewBag.Supe_Administrador);

#line default
#line hidden
#nullable disable
                WriteLiteral("</title>\r\n\r\n");
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.HeadTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_HeadTagHelper);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n\r\n<h2>Detalles</h2>\r\n<hr />\r\n<div id=\"conten\">\r\n\r\n    <div class=\"dl-horizontal\">\r\n        <h1>\r\n            Permiso\r\n        </h1>\r\n        <label>\r\n            ");
#nullable restore
#line 24 "C:\Users\ANA ROJAS\Source\Repos\desarrollo-web-tobe\TobeAppWeb\TobeAppWeb\Views\Asp_Net_Users\Details.cshtml"
       Write(Html.DisplayFor(model => model.Permiso));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </label>\r\n        <h1>\r\n            Nombre de usuario\r\n        </h1>\r\n        <label>\r\n            ");
#nullable restore
#line 30 "C:\Users\ANA ROJAS\Source\Repos\desarrollo-web-tobe\TobeAppWeb\TobeAppWeb\Views\Asp_Net_Users\Details.cshtml"
       Write(Html.DisplayFor(model => model.UserName));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </label>\r\n        <h1>\r\n            Nombre de usuario normalizado\r\n        </h1>\r\n        <label>\r\n            ");
#nullable restore
#line 36 "C:\Users\ANA ROJAS\Source\Repos\desarrollo-web-tobe\TobeAppWeb\TobeAppWeb\Views\Asp_Net_Users\Details.cshtml"
       Write(Html.DisplayFor(model => model.NormalizedUserName));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </label>\r\n        <h1>\r\n            Correo electrónico\r\n        </h1>\r\n        <label>\r\n            ");
#nullable restore
#line 42 "C:\Users\ANA ROJAS\Source\Repos\desarrollo-web-tobe\TobeAppWeb\TobeAppWeb\Views\Asp_Net_Users\Details.cshtml"
       Write(Html.DisplayFor(model => model.Email));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </label>\r\n        <h1>\r\n            Correo normalizado\r\n        </h1>\r\n        <label>\r\n            ");
#nullable restore
#line 48 "C:\Users\ANA ROJAS\Source\Repos\desarrollo-web-tobe\TobeAppWeb\TobeAppWeb\Views\Asp_Net_Users\Details.cshtml"
       Write(Html.DisplayFor(model => model.NormalizedEmail));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </label>\r\n        <h1>\r\n            Correo confirmado\r\n        </h1>\r\n        <label>\r\n            ");
#nullable restore
#line 54 "C:\Users\ANA ROJAS\Source\Repos\desarrollo-web-tobe\TobeAppWeb\TobeAppWeb\Views\Asp_Net_Users\Details.cshtml"
       Write(Html.DisplayFor(model => model.EmailConfirmed));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </label>\r\n        <h1>\r\n            Constraseña\r\n        </h1>\r\n        <label>\r\n            ");
#nullable restore
#line 60 "C:\Users\ANA ROJAS\Source\Repos\desarrollo-web-tobe\TobeAppWeb\TobeAppWeb\Views\Asp_Net_Users\Details.cshtml"
       Write(Html.DisplayFor(model => model.PasswordHash));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </label>\r\n        <h1>\r\n            Sello de seguridad\r\n        </h1>\r\n        <label>\r\n            ");
#nullable restore
#line 66 "C:\Users\ANA ROJAS\Source\Repos\desarrollo-web-tobe\TobeAppWeb\TobeAppWeb\Views\Asp_Net_Users\Details.cshtml"
       Write(Html.DisplayFor(model => model.SecurityStamp));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </label>\r\n        <h1>\r\n            Sello de concurrencia\r\n        </h1>\r\n        <label>\r\n            ");
#nullable restore
#line 72 "C:\Users\ANA ROJAS\Source\Repos\desarrollo-web-tobe\TobeAppWeb\TobeAppWeb\Views\Asp_Net_Users\Details.cshtml"
       Write(Html.DisplayFor(model => model.ConcurrencyStamp));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </label>\r\n        <h1>\r\n            Teléfono\r\n        </h1>\r\n        <label>\r\n            ");
#nullable restore
#line 78 "C:\Users\ANA ROJAS\Source\Repos\desarrollo-web-tobe\TobeAppWeb\TobeAppWeb\Views\Asp_Net_Users\Details.cshtml"
       Write(Html.DisplayFor(model => model.PhoneNumber));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </label>\r\n        <h1>\r\n            Teléfono confirmado\r\n        </h1>\r\n        <label>\r\n            ");
#nullable restore
#line 84 "C:\Users\ANA ROJAS\Source\Repos\desarrollo-web-tobe\TobeAppWeb\TobeAppWeb\Views\Asp_Net_Users\Details.cshtml"
       Write(Html.DisplayFor(model => model.PhoneNumberConfirmed));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </label>\r\n        <h1>\r\n            Autenticación de doble factor\r\n        </h1>\r\n        <label>\r\n            ");
#nullable restore
#line 90 "C:\Users\ANA ROJAS\Source\Repos\desarrollo-web-tobe\TobeAppWeb\TobeAppWeb\Views\Asp_Net_Users\Details.cshtml"
       Write(Html.DisplayFor(model => model.TwoFactorEnabled));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </label>\r\n        <h1>\r\n            Fin de bloqueo\r\n        </h1>\r\n        <label>\r\n            ");
#nullable restore
#line 96 "C:\Users\ANA ROJAS\Source\Repos\desarrollo-web-tobe\TobeAppWeb\TobeAppWeb\Views\Asp_Net_Users\Details.cshtml"
       Write(Html.DisplayFor(model => model.LockoutEnd));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </label>\r\n        <h1>\r\n            Fin de bloqueo habilitado\r\n        </h1>\r\n        <label>\r\n            ");
#nullable restore
#line 102 "C:\Users\ANA ROJAS\Source\Repos\desarrollo-web-tobe\TobeAppWeb\TobeAppWeb\Views\Asp_Net_Users\Details.cshtml"
       Write(Html.DisplayFor(model => model.LockoutEnabled));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </label>\r\n        <h1>\r\n            Contador de ingresos fallidos\r\n        </h1>\r\n        <label>\r\n            ");
#nullable restore
#line 108 "C:\Users\ANA ROJAS\Source\Repos\desarrollo-web-tobe\TobeAppWeb\TobeAppWeb\Views\Asp_Net_Users\Details.cshtml"
       Write(Html.DisplayFor(model => model.AccessFailedCount));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </label>\r\n        <h1>\r\n            Entidad\r\n        </h1>\r\n        <label>\r\n            ");
#nullable restore
#line 114 "C:\Users\ANA ROJAS\Source\Repos\desarrollo-web-tobe\TobeAppWeb\TobeAppWeb\Views\Asp_Net_Users\Details.cshtml"
       Write(Html.DisplayFor(model => model.IdEntidadNavigation.Entidad));

#line default
#line hidden
#nullable disable
            WriteLiteral("\r\n        </label><br /><br />\r\n    </div>\r\n    <div>\r\n        ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "2408d07b6b41e46a23c681e97118059e3db383df12282", async() => {
                WriteLiteral("Editar");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_0.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_0);
            if (__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues == null)
            {
                throw new InvalidOperationException(InvalidTagHelperIndexerAssignment("asp-route-id", "Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper", "RouteValues"));
            }
            BeginWriteTagHelperAttribute();
#nullable restore
#line 118 "C:\Users\ANA ROJAS\Source\Repos\desarrollo-web-tobe\TobeAppWeb\TobeAppWeb\Views\Asp_Net_Users\Details.cshtml"
                               WriteLiteral(Model.Id);

#line default
#line hidden
#nullable disable
            __tagHelperStringValueBuffer = EndWriteTagHelperAttribute();
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"] = __tagHelperStringValueBuffer;
            __tagHelperExecutionContext.AddTagHelperAttribute("asp-route-id", __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.RouteValues["id"], global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(" |\r\n        ");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("a", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "2408d07b6b41e46a23c681e97118059e3db383df14465", async() => {
                WriteLiteral("Volver");
            }
            );
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.TagHelpers.AnchorTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper);
            __Microsoft_AspNetCore_Mvc_TagHelpers_AnchorTagHelper.Action = (string)__tagHelperAttribute_1.Value;
            __tagHelperExecutionContext.AddTagHelperAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n    </div>\r\n</div>\r\n\r\n");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<TobeAppWeb.Models.AspNetUsers> Html { get; private set; }
    }
}
#pragma warning restore 1591
