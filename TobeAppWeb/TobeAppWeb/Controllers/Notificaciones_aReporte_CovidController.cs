﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TobeAppWeb.Models;

namespace TobeAppWeb.Controllers
{
    [Authorize(Roles = "Admin")]
    public class Notificaciones_aReporte_CovidController : Controller
    {
        private readonly AppTobeContext _context;

        public Notificaciones_aReporte_CovidController(AppTobeContext context)
        {
            _context = context;
        }

        // GET: NotificacionesaReporteCovids
         public async Task<IActionResult> Index()
         {
             var datosUsua = await _context.AspNetUsers.Include(s => s.IdEntidadNavigation).SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
             var entidad = await _context.Entidades.SingleOrDefaultAsync(s => s.Id == datosUsua.IdEntidad);
             var appTobeContext = _context.NotificacionesaReporteCovid.Include(n => n.IdReporteCov.IdUsuarioNavigation).Where(s => s.EntidadNotifica == datosUsua.IdEntidadNavigation.Entidad);
             return View(await appTobeContext.ToListAsync());
         }

        // GET: NotificacionesaReporteCovids/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notificacionesaReporteCovid = await _context.NotificacionesaReporteCovid
                .Include(n => n.IdReporteCov)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (notificacionesaReporteCovid == null)
            {
                return NotFound();
            }
            var datosUsua = await _context.AspNetUsers.Include(s => s.IdEntidadNavigation).SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
            if (notificacionesaReporteCovid.EntidadNotifica != (datosUsua.IdEntidadNavigation.Entidad))
            {
                return NotFound();
            }
            return View(notificacionesaReporteCovid);
        }

        // GET: NotificacionesaReporteCovids/Create
        public IActionResult Create()
        {
            ViewData["IdReporteCovid"] = new SelectList(_context.ReporteCovid, "Id", "Id");
            ViewData["IdUsuario"] = new SelectList(_context.Usuarios, "Id", "Nombredeusuario");
            return View();
        }

        // POST: NotificacionesaReporteCovids/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,IdUsuario,IdReporteCovid,EntidadNotifica,FechaNotifica,IraTrabajar,CitaTelemedicina")] NotificacionesaReporteCovid notificacionesaReporteCovid)
        {
            if (ModelState.IsValid)
            {
                _context.Add(notificacionesaReporteCovid);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdReporteCovid"] = new SelectList(_context.ReporteCovid, "Id", "Id", notificacionesaReporteCovid.IdReporteCovid);
         //   ViewData["IdUsuario"] = new SelectList(_context.Usuarios, "Id", "Nombredeusuario", notificacionesaReporteCovid.IdUsuario);
            return View(notificacionesaReporteCovid);
        }

        // GET: NotificacionesaReporteCovids/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notificacionesaReporteCovid = await _context.NotificacionesaReporteCovid.FindAsync(id);
            if (notificacionesaReporteCovid == null)
            {
                return NotFound();
            }
            var datosUsua = await _context.AspNetUsers.Include(s => s.IdEntidadNavigation).SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
            if (notificacionesaReporteCovid.EntidadNotifica != (datosUsua.IdEntidadNavigation.Entidad))
            {
                return NotFound();
            }
            ViewData["IdReporteCovid"] = new SelectList(_context.ReporteCovid, "Id", "Id", notificacionesaReporteCovid.IdReporteCovid);
           // ViewData["IdUsuario"] = new SelectList(_context.Usuarios, "Id", "Nombredeusuario", notificacionesaReporteCovid.IdUsuario);
            return View(notificacionesaReporteCovid);
        }

        // POST: NotificacionesaReporteCovids/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,IdUsuario,IdReporteCovid,EntidadNotifica,FechaNotifica,IraTrabajar,CitaTelemedicina")] NotificacionesaReporteCovid notificacionesaReporteCovid)
        {
            if (id != notificacionesaReporteCovid.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(notificacionesaReporteCovid);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NotificacionesaReporteCovidExists(notificacionesaReporteCovid.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdReporteCovid"] = new SelectList(_context.ReporteCovid, "Id", "Id", notificacionesaReporteCovid.IdReporteCovid);
           // ViewData["IdUsuario"] = new SelectList(_context.Usuarios, "Id", "Nombredeusuario", notificacionesaReporteCovid.IdUsuario);
            return View(notificacionesaReporteCovid);
        }

        // GET: NotificacionesaReporteCovids/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var notificacionesaReporteCovid = await _context.NotificacionesaReporteCovid
                .Include(n => n.IdReporteCov)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (notificacionesaReporteCovid == null)
            {
                return NotFound();
            }
            var datosUsua = await _context.AspNetUsers.Include(s => s.IdEntidadNavigation).SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
            if (notificacionesaReporteCovid.EntidadNotifica != (datosUsua.IdEntidadNavigation.Entidad))
            {
                return NotFound();
            }

            return View(notificacionesaReporteCovid);
        }

        // POST: NotificacionesaReporteCovids/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var notificacionesaReporteCovid = await _context.NotificacionesaReporteCovid.FindAsync(id);
            _context.NotificacionesaReporteCovid.Remove(notificacionesaReporteCovid);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool NotificacionesaReporteCovidExists(int id)
        {
            return _context.NotificacionesaReporteCovid.Any(e => e.Id == id);
        }
    }
}
