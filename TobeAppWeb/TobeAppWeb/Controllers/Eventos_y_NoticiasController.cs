﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TobeAppWeb.Modelos;
using TobeAppWeb.Models;

namespace TobeAppWeb.Controllers
{
    [Authorize(Roles = "Admin")]
    public class Eventos_y_NoticiasController : Controller
    {
        private readonly AppTobeContext _context;

        public Eventos_y_NoticiasController(AppTobeContext context)
        {
            _context = context;
        }

        // GET: EventosyNoticias
        public async Task<IActionResult> Index()
        {
            var datosUsua = await _context.AspNetUsers.SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
            var appTobeContext = _context.EventosyNoticias.Include(e => e.IdSedeNavigation).Where(s => s.IdSedeNavigation.IdEntidad == datosUsua.IdEntidad); 
            return View(await appTobeContext.ToListAsync());
        }

        // GET: EventosyNoticias/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var eventosyNoticias = await _context.EventosyNoticias
                .Include(e => e.IdSedeNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (eventosyNoticias == null)
            {
                return NotFound();
            }
            var datosUsua = await _context.AspNetUsers.SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
            if (eventosyNoticias.IdSedeNavigation.IdEntidad != datosUsua.IdEntidad)
            {
                return NotFound();
            }

            return View(eventosyNoticias);
        }

        // GET: EventosyNoticias/Create
        public async Task<IActionResult> CreateAsync()
        {
            List<int> tipo = new List<int>();
            tipo.Add(1);
            tipo.Add(2);
            tipo.Add(3);
            ViewData["Tipo"] = new SelectList(tipo);
            var datosUsua = await _context.AspNetUsers.SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
            ViewData["IdSede"] = new SelectList(_context.Sedes.Where(s => s.IdEntidad == datosUsua.IdEntidad), "Id", "NombreSede");
            return View();
        }

        // POST: EventosyNoticias/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CrearEvento eventosyNoticias)
        {

            var formData = new MultipartFormDataContent();
            using (var fileStream = eventosyNoticias.Imagen.OpenReadStream())
            {
                HttpClient conex = new HttpClient();
                conex.Timeout = TimeSpan.FromMilliseconds(20000);
                conex.MaxResponseContentBufferSize = 256000;
                conex.BaseAddress = new Uri("http://apptobe.us-east-2.elasticbeanstalk.com/api/");
                conex.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                formData.Add(new StreamContent(fileStream), "MyImage", eventosyNoticias.Imagen.FileName);
                var response = await conex.PostAsync("Entidades/SubirImagen", formData).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    var json = await response.Content.ReadAsStringAsync();
                    if (ModelState.IsValid)
                    {
                        EventosyNoticias nn = new EventosyNoticias();
                        nn.Titulo = eventosyNoticias.Titulo;
                        nn.IdSede = eventosyNoticias.IdSede; 
                        nn.Descripcion = eventosyNoticias.Descripcion;
                        nn.Tipo = eventosyNoticias.Tipo;
                        nn.Imagen = json;
                        _context.Add(nn);
                        await _context.SaveChangesAsync();
                        return RedirectToAction(nameof(Index));
                    }
                }
            }
            List<int> tipo = new List<int>();
            tipo.Add(1);
            tipo.Add(2);
            tipo.Add(3);
            ViewData["Tipo"] = new SelectList(tipo);
            var datosUsua = await _context.AspNetUsers.SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
            ViewData["IdSede"] = new SelectList(_context.Sedes.Where(s => s.IdEntidad == datosUsua.IdEntidad), "Id", "NombreSede", eventosyNoticias.IdSede);
            return View(eventosyNoticias);
        }

        // GET: EventosyNoticias/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            // var eventosyNoticias = await _context.EventosyNoticias.FindAsync(id);
            var eventosyNoticias = await _context.EventosyNoticias
                 .Include(e => e.IdSedeNavigation)
                 .FirstOrDefaultAsync(m => m.Id == id);
            if (eventosyNoticias == null)
            {
                return NotFound();
            }
            var datosUsua = await _context.AspNetUsers.SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
            if (eventosyNoticias.IdSedeNavigation.IdEntidad != datosUsua.IdEntidad)
            {
                return NotFound();
            }
            List<int> tipo = new List<int>();
            tipo.Add(1);
            tipo.Add(2);
            tipo.Add(3);
            ViewData["Tipo"] = new SelectList(tipo);
            ViewData["IdSede"] = new SelectList(_context.Sedes.Where(s => s.IdEntidad == datosUsua.IdEntidad), "Id", "NombreSede", eventosyNoticias.IdSede);
            return View(eventosyNoticias);
        }

        // POST: EventosyNoticias/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,IdSede,Tipo,Titulo,Descripcion,Imagen")] EventosyNoticias eventosyNoticias)
        {
            if (id != eventosyNoticias.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(eventosyNoticias);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EventosyNoticiasExists(eventosyNoticias.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            List<int> tipo = new List<int>();
            tipo.Add(1);
            tipo.Add(2);
            tipo.Add(3);
            ViewData["Tipo"] = new SelectList(tipo);
            var datosUsua = await _context.AspNetUsers.SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
            ViewData["IdSede"] = new SelectList(_context.Sedes.Where(s => s.IdEntidad == datosUsua.IdEntidad), "Id", "NombreSede", eventosyNoticias.IdSede);
            return View(eventosyNoticias);
        }

        // GET: EventosyNoticias/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var eventosyNoticias = await _context.EventosyNoticias
                .Include(e => e.IdSedeNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (eventosyNoticias == null)
            {
                return NotFound();
            }
            var datosUsua = await _context.AspNetUsers.SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
            if (eventosyNoticias.IdSedeNavigation.IdEntidad != datosUsua.IdEntidad)
            {
                return NotFound();
            }

            return View(eventosyNoticias);
        }

        // POST: EventosyNoticias/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var eventosyNoticias = await _context.EventosyNoticias.FindAsync(id);
            _context.EventosyNoticias.Remove(eventosyNoticias);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EventosyNoticiasExists(int id)
        {
            return _context.EventosyNoticias.Any(e => e.Id == id);
        }
    }
}
