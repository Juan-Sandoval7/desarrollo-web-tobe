﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TobeAppWeb.Modelos;
using TobeAppWeb.Models;

namespace TobeAppWeb.Controllers
{
    [Authorize(Roles = "Admin")]
    public class Sedes_Controller : Controller
    {
       
        private readonly AppTobeContext _context;

        public Sedes_Controller(AppTobeContext context)
        {
            _context = context;
        }

        // GET: Sedes
        public async Task<IActionResult> Index()
        {
            var datosUsua = await _context.AspNetUsers.SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
            var appTobeContext = _context.Sedes.Include(s => s.IdEntidadNavigation).Where(s => s.IdEntidad == datosUsua.IdEntidad);
            return View(await appTobeContext.ToListAsync());
        }

        // GET: Sedes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            var datosUsua = await _context.AspNetUsers.SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
            if (id == null)
            {
                return NotFound();
            }

            var sedes = await _context.Sedes
                .Include(s => s.IdEntidadNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (sedes == null)
            {
                return NotFound();
            }
            if (sedes.IdEntidad != datosUsua.IdEntidad)
            {
                return NotFound();
            }
           

            return View(sedes);
        }

        // GET: Sedes/Create
        public async Task<IActionResult> CreateAsync()
        {
            var datosUsua = await _context.AspNetUsers.SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
            ViewData["IdEntidad"] = new SelectList(_context.Entidades.Where(s => s.Id == datosUsua.IdEntidad), "Id", "Entidad");
            return View();
        }

        // POST: Sedes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,IdEntidad,NombreSede,Pais,Departamento,Ciudad,Direccion,Latitud,Longitud")] Sedes sedes)
        {
            StringBuilder sb = new StringBuilder();
            var confirm = await _context.Sedes.SingleOrDefaultAsync(s => s.NombreSede == sedes.NombreSede);
            if (confirm == null)
            {

                if (ModelState.IsValid)
                {
                    _context.Add(sedes);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }
            else
            {
                TempData["Mensaje"] = "El nombre de la sede ya esta registrado";
                return RedirectToAction("Create");
            }
            var datosUsua = await _context.AspNetUsers.SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
            ViewData["IdEntidad"] = new SelectList(_context.Entidades.Where(s => s.Id == datosUsua.IdEntidad), "Id", "Entidad", sedes.IdEntidad);
            return View(sedes);
        }

        // GET: Sedes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sedes = await _context.Sedes.FindAsync(id);
            if (sedes == null)
            {
                return NotFound();
            }
            var datosUsua = await _context.AspNetUsers.SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
            if (sedes.IdEntidad != datosUsua.IdEntidad)
            {
                return NotFound();
            }

            ViewData["IdEntidad"] = new SelectList(_context.Entidades.Where(s => s.Id == datosUsua.IdEntidad), "Id", "Entidad", sedes.IdEntidad);
            return View(sedes);
        }

        // POST: Sedes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,IdEntidad,NombreSede,Pais,Departamento,Ciudad,Direccion,Latitud,Longitud")] Sedes sedes)
        {
            if (id != sedes.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    try
                    {
                        _context.Update(sedes);
                        await _context.SaveChangesAsync();
                    }
                    catch(Exception e)
                    {
                        TempData["Mensaje"] = "El nombre de la sede ya esta registrado";
                        return RedirectToAction("Index");
                    }
                        
                       
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SedesExists(sedes.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                          TempData["Mensaje"] = "El nombre usuario o correo ya esta registrado";
                          return RedirectToAction("Index");
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            var datosUsua = await _context.AspNetUsers.SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
            ViewData["IdEntidad"] = new SelectList(_context.Entidades.Where(s => s.Id == datosUsua.IdEntidad), "Id", "Entidad", sedes.IdEntidad);
            return View(sedes);
        }

        // GET: Sedes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var sedes = await _context.Sedes
                .Include(s => s.IdEntidadNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (sedes == null)
            {
                return NotFound();
            }
            var datosUsua = await _context.AspNetUsers.SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
            if (sedes.IdEntidad != datosUsua.IdEntidad)
            {
                return NotFound();
            }

            return View(sedes);
        }

        // POST: Sedes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var sedes = await _context.Sedes.FindAsync(id);
            _context.Sedes.Remove(sedes);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SedesExists(int id)
        {
            return _context.Sedes.Any(e => e.Id == id);
        }
    }
}
