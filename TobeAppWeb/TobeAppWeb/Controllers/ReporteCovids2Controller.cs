﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TobeAppWeb.Models;

namespace TobeAppWeb.Controllers
{
    public class ReporteCovids2Controller : Controller
    {
        private readonly AppTobeContext _context;

        public ReporteCovids2Controller(AppTobeContext context)
        {
            _context = context;
        }

        // GET: ReporteCovids2
        public async Task<IActionResult> Index()
        {
            var appTobeContext = _context.ReporteCovid.Include(r => r.IdSedesNavigation).Include(r => r.IdUsuarioNavigation);
            return View(await appTobeContext.ToListAsync());
        }

        // GET: ReporteCovids2/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var reporteCovid = await _context.ReporteCovid
                .Include(r => r.IdSedesNavigation)
                .Include(r => r.IdUsuarioNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (reporteCovid == null)
            {
                return NotFound();
            }

            return View(reporteCovid);
        }

        // GET: ReporteCovids2/Create
        public IActionResult Create()
        {
            ViewData["IdSedes"] = new SelectList(_context.Sedes, "Id", "Ciudad");
            ViewData["IdUsuario"] = new SelectList(_context.Usuarios, "Id", "Contraseña");
            return View();
        }

        // POST: ReporteCovids2/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,IdUsuario,IdSedes,Nombre,NumerodeIdentificacion,Fecha,AreadeTrabajo,Sintomas,SintomasFamilia,Rta6,Rta7,Rta8,Rta9,Estadodeanimo,Enfermedades,Iratrabajar,Comentarios,NumerodeSintomas,Notificado,NotificadoU")] ReporteCovid reporteCovid)
        {
            if (ModelState.IsValid)
            {
                _context.Add(reporteCovid);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdSedes"] = new SelectList(_context.Sedes, "Id", "Ciudad", reporteCovid.IdSedes);
            ViewData["IdUsuario"] = new SelectList(_context.Usuarios, "Id", "Contraseña", reporteCovid.IdUsuario);
            return View(reporteCovid);
        }

        // GET: ReporteCovids2/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var reporteCovid = await _context.ReporteCovid.FindAsync(id);
            if (reporteCovid == null)
            {
                return NotFound();
            }
            ViewData["IdSedes"] = new SelectList(_context.Sedes, "Id", "Ciudad", reporteCovid.IdSedes);
            ViewData["IdUsuario"] = new SelectList(_context.Usuarios, "Id", "Contraseña", reporteCovid.IdUsuario);
            return View(reporteCovid);
        }

        // POST: ReporteCovids2/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,IdUsuario,IdSedes,Nombre,NumerodeIdentificacion,Fecha,AreadeTrabajo,Sintomas,SintomasFamilia,Rta6,Rta7,Rta8,Rta9,Estadodeanimo,Enfermedades,Iratrabajar,Comentarios,NumerodeSintomas,Notificado,NotificadoU")] ReporteCovid reporteCovid)
        {
            if (id != reporteCovid.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(reporteCovid);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ReporteCovidExists(reporteCovid.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdSedes"] = new SelectList(_context.Sedes, "Id", "Ciudad", reporteCovid.IdSedes);
            ViewData["IdUsuario"] = new SelectList(_context.Usuarios, "Id", "Contraseña", reporteCovid.IdUsuario);
            return View(reporteCovid);
        }

        // GET: ReporteCovids2/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var reporteCovid = await _context.ReporteCovid
                .Include(r => r.IdSedesNavigation)
                .Include(r => r.IdUsuarioNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (reporteCovid == null)
            {
                return NotFound();
            }

            return View(reporteCovid);
        }

        // POST: ReporteCovids2/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var reporteCovid = await _context.ReporteCovid.FindAsync(id);
            _context.ReporteCovid.Remove(reporteCovid);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ReporteCovidExists(int id)
        {
            return _context.ReporteCovid.Any(e => e.Id == id);
        }
    }
}
