﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TobeAppWeb.Models;

namespace TobeAppWeb.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SubcategoriasController : ControllerBase
    {
        private readonly AppTobeContext _context;

        public SubcategoriasController(AppTobeContext context)
        {
            _context = context;
        }

        // GET: api/Subcategorias
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Subcategorias>>> GetSubcategorias()
        {
            return await _context.Subcategorias.ToListAsync();
        }

        // GET: api/Subcategorias/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Subcategorias>> GetSubcategorias(int id)
        {
            var subcategorias = await _context.Subcategorias.FindAsync(id);

            if (subcategorias == null)
            {
                return NotFound();
            }

            return subcategorias;
        }

        // PUT: api/Subcategorias/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutSubcategorias(int id, Subcategorias subcategorias)
        {
            if (id != subcategorias.Id)
            {
                return BadRequest();
            }

            _context.Entry(subcategorias).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubcategoriasExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Subcategorias
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Subcategorias>> PostSubcategorias(Subcategorias subcategorias)
        {
            _context.Subcategorias.Add(subcategorias);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSubcategorias", new { id = subcategorias.Id }, subcategorias);
        }

        // DELETE: api/Subcategorias/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Subcategorias>> DeleteSubcategorias(int id)
        {
            var subcategorias = await _context.Subcategorias.FindAsync(id);
            if (subcategorias == null)
            {
                return NotFound();
            }

            _context.Subcategorias.Remove(subcategorias);
            await _context.SaveChangesAsync();

            return subcategorias;
        }

        private bool SubcategoriasExists(int id)
        {
            return _context.Subcategorias.Any(e => e.Id == id);
        }
    }
}
