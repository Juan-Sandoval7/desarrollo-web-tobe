﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TobeAppWeb.Models;

namespace TobeAppWeb.Controllers
{
    [Authorize(Roles = "Admin")]
    public class Permiso_UsuariosController : Controller
    {
        private readonly AppTobeContext _context;

        public Permiso_UsuariosController(AppTobeContext context)
        {
            _context = context;
        }
       
        // GET: PermisoUsuarios
        public async Task<IActionResult> Index()
        {
            var datosUsua = await _context.AspNetUsers.SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
            var appTobeContext = _context.PermisoUsuarios.Include(p => p.IdEntidadNavigation).Include(p => p.IdUsuariosNavigation).Where(s => s.IdEntidad == datosUsua.IdEntidad);
            return View(await appTobeContext.ToListAsync());
        }

    


        // GET: PermisoUsuarios/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var permisoUsuarios = await _context.PermisoUsuarios
                .Include(p => p.IdEntidadNavigation)
                .Include(p => p.IdUsuariosNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (permisoUsuarios == null)
            {
                return NotFound();
            }
            var datosUsua = await _context.AspNetUsers.SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
            if (permisoUsuarios.IdEntidad != datosUsua.IdEntidad)
            {
                return NotFound();
            }


            return View(permisoUsuarios);
        }

        // GET: PermisoUsuarios/Create
        public async Task<IActionResult> CreateAsync()
        {
            var datosUsua = await _context.AspNetUsers.Include(s => s.IdEntidadNavigation).SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
           ViewData["IdEntidad"] = new SelectList(_context.Entidades.Where(s => s.Id == datosUsua.IdEntidad), "Id", "Entidad");
            ViewData["IdUsuarios"] = new SelectList(_context.Usuarios, "Id", "Nombredeusuario");
            return View();
        }

        // POST: PermisoUsuarios/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,IdUsuarios,IdEntidad")] PermisoUsuarios permisoUsuarios)
        {
            if (ModelState.IsValid)
            {
                var comprobarpermiso = await _context.PermisoUsuarios.SingleOrDefaultAsync(s => s.IdUsuarios == permisoUsuarios.IdUsuarios && s.IdEntidad == permisoUsuarios.IdEntidad);
                if (comprobarpermiso == null)
                {
                    _context.Add(permisoUsuarios);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
            }
            var datosUsua = await _context.AspNetUsers.Include(s => s.IdEntidadNavigation).SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
            ViewData["IdEntidad"] = new SelectList(_context.Entidades.Where(s => s.Id == datosUsua.IdEntidad), "Id", "Entidad", permisoUsuarios.IdEntidad);
            ViewData["IdUsuarios"] = new SelectList(_context.Usuarios, "Id", "Nombredeusuario", permisoUsuarios.IdUsuarios);
            return View(permisoUsuarios);
        }

        // GET: PermisoUsuarios/Edit/5
 

        // GET: PermisoUsuarios/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var permisoUsuarios = await _context.PermisoUsuarios
                .Include(p => p.IdEntidadNavigation)
                .Include(p => p.IdUsuariosNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (permisoUsuarios == null)
            {
                return NotFound();
            }
            var datosUsua = await _context.AspNetUsers.SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
            if (permisoUsuarios.IdEntidad != datosUsua.IdEntidad)
            {
                return NotFound();
            }

            return View(permisoUsuarios);
        }

        // POST: PermisoUsuarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var permisoUsuarios = await _context.PermisoUsuarios.FindAsync(id);
            _context.PermisoUsuarios.Remove(permisoUsuarios);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PermisoUsuariosExists(int id)
        {
            return _context.PermisoUsuarios.Any(e => e.Id == id);
        }
    }
}
