﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ImportExcelFIle.DotNETCore.Models;
using Microsoft.AspNetCore.Hosting;
using System.Text;
using System.IO;
using Microsoft.AspNetCore.Http;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using TobeAppWeb.Models;
using NPOI.SS.Formula.Functions;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;
using System.Net.Http;
using System.Net.Http.Headers;
using TobeAppWeb.Modelos;

namespace TobeAppWeb.Controllers
{
    [Authorize(Roles = "SuperAdmin")]
    public class EntidadController : Controller
    {
        private readonly AppTobeContext _context;
        private readonly ILogger<EntidadController> _logger;
        private IHostingEnvironment _hostingEnvironment;

        public EntidadController(ILogger<EntidadController> logger, IHostingEnvironment hostingEnvironment, AppTobeContext context)
        {
            _logger = logger;
            _hostingEnvironment = hostingEnvironment;
            _context = context;
        }

        // GET: Entidad
        public async Task<IActionResult> Index()
        {
            return View(await _context.Entidades.ToListAsync());
        }

        // GET: Entidad/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var entidades = await _context.Entidades
                .FirstOrDefaultAsync(m => m.Id == id);
            if (entidades == null)
            {
                return NotFound();
            }

            return View(entidades);
        }

        // GET: Entidad/Create
      
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(CrearEntidades entidades)
        {
            var confirm = await _context.Entidades.SingleOrDefaultAsync(s => s.Entidad == entidades.Entidad);
            if (confirm == null)
            {
                var formData = new MultipartFormDataContent();
                using (var fileStream = entidades.Logo.OpenReadStream())
                {
                    HttpClient conex = new HttpClient();
                    conex.Timeout = TimeSpan.FromMilliseconds(20000);
                    conex.MaxResponseContentBufferSize = 256000;
                    conex.BaseAddress = new Uri("http://apptobe.us-east-2.elasticbeanstalk.com/api/");
                    conex.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    formData.Add(new StreamContent(fileStream), "MyImage", entidades.Logo.FileName);
                    var response = await conex.PostAsync("Entidades/SubirImagen", formData).ConfigureAwait(false);

                    if (response.IsSuccessStatusCode)
                    {
                        var json = await response.Content.ReadAsStringAsync();
                        if (ModelState.IsValid)
                        {
                            Entidades nn = new Entidades();
                            nn.Entidad = entidades.Entidad;
                            nn.Descripcion = entidades.Descripcion;
                            nn.Color = entidades.Color;
                            nn.Logo = json.ToString().Replace('"', ' ').Trim();
                            nn.LinkReportes = entidades.LinkReportes;
                            _context.Add(nn);
                            await _context.SaveChangesAsync();
                            return RedirectToAction(nameof(Index));
                        }

                    }
                }
            }
            else
            {
                TempData["Mensaje"] = "El nombre de la entidad ya esta registrado";
                return RedirectToAction("Create");
            }

            return View(entidades);
        }

        // GET: Entidad/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var entidades = await _context.Entidades.FindAsync(id);
            if (entidades == null)
            {
                return NotFound();
            }
            return View(entidades);
        }

        // POST: Entidad/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Entidad,Descripcion,Logo,Color,LinkReportes")] Entidades entidades)
        {
            if (id != entidades.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    try
                    {
                        _context.Update(entidades);
                        await _context.SaveChangesAsync();
                    }
                    catch (Exception e)
                    
                    {
                    TempData["Mensaje"] = "El nombre de la entidad ya esta registrado";
                    return RedirectToAction("Index");
                     }
              }
                catch (DbUpdateConcurrencyException)
                {
                    if (!EntidadesExists(entidades.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(entidades);
        }

        // GET: Entidad/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var entidades = await _context.Entidades
                .FirstOrDefaultAsync(m => m.Id == id);
            if (entidades == null)
            {
                return NotFound();
            }

            return View(entidades);
        }

        // POST: Entidad/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var entidades = await _context.Entidades.FindAsync(id);
            _context.Entidades.Remove(entidades);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool EntidadesExists(int id)
        {
            return _context.Entidades.Any(e => e.Id == id);
        }

        public async Task<IActionResult> Import()
        {
            String[] valores;
            valores = new String[9];

            IFormFile file = Request.Form.Files[0];
            string folderName = "UploadImages";
            string webRootPath = _hostingEnvironment.WebRootPath;
            string newPath = Path.Combine(webRootPath, folderName);
            StringBuilder sb = new StringBuilder();

            using (var formData = new MultipartFormDataContent())
            {
                HttpClient conex = new HttpClient();
                conex.Timeout = TimeSpan.FromMilliseconds(20000);
                conex.MaxResponseContentBufferSize = 256000;
                conex.BaseAddress = new Uri("http://apptobe.us-east-2.elasticbeanstalk.com/api/");
                conex.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                formData.Add((HttpContent)file, "MyImage", "Enti");
                var response = await conex.PostAsync("Entidades/SubirImagen", formData).ConfigureAwait(false);

                if (response.IsSuccessStatusCode)
                {
                    var json = await response.Content.ReadAsStringAsync();
                    Entidades nn = new Entidades();
                    nn.Entidad = "NN";
                    nn.Descripcion = "NN";
                    nn.Color = "NN";
                    nn.Logo = json;
                    _context.Add(nn);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));


                }
            }



            if (!Directory.Exists(newPath))
            {
                Directory.CreateDirectory(newPath);
            }
            if (file.Length > 0)
            {
                string sFileExtension = Path.GetExtension(file.FileName).ToLower();
                ISheet sheet;
                string fullPath = Path.Combine(newPath, file.FileName);

                using (var stream = new FileStream(fullPath, FileMode.Create))
                {
                    file.CopyTo(stream);
                    stream.Position = 0;
                    if (sFileExtension == ".xls")
                    {
                        HSSFWorkbook hssfwb = new HSSFWorkbook(stream); //This will read the Excel 97-2000 formats  
                        sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook  
                    }
                    else
                    {
                        XSSFWorkbook hssfwb = new XSSFWorkbook(stream); //This will read 2007 Excel format  
                        sheet = hssfwb.GetSheetAt(0); //get first sheet from workbook   
                    }


                    IRow headerRow = sheet.GetRow(0); //Get Header Row
                    int cellCount = headerRow.LastCellNum;
                    sb.Append("<table class='table table-bordered'><tr>");
                    for (int j = 0; j < 9; j++)
                    {
                        NPOI.SS.UserModel.ICell cell = headerRow.GetCell(j);
                        if (cell == null || string.IsNullOrWhiteSpace(cell.ToString()))
                        {
                            sb.Append("<th> </th>");

                        }
                        else
                        {
                            sb.Append("<th>" + cell.ToString() + "</th>");
                        }

                    }
                    sb.Append("</tr>");
                    sb.AppendLine("<tr>");


                    for (int i = (sheet.FirstRowNum + 1); i <= sheet.LastRowNum; i++) //Read Excel File
                    {

                        IRow row = sheet.GetRow(i);
                        if (row == null) continue;
                        if (row.Cells.All(d => d.CellType == CellType.Blank)) continue;
                        for (int j = row.FirstCellNum; j < 9; j++)
                        {
                            if (row.GetCell(j) != null)
                            {
                                sb.Append("<td>" + row.GetCell(j).ToString() + "</td>");
                                valores[j] = row.GetCell(j).ToString();
                            }
                            else
                            {
                                sb.Append("<th> </th>");
                                valores[j] = " ";
                            }

                        }
                        Usuarios Valores = new Usuarios();
                        Valores.Nombre = valores[0];
                        Valores.Nombredeusuario = valores[1];
                        Valores.Contraseña = valores[2];
                        Valores.Correo = valores[3];
                        Valores.Telefono = valores[4];
                        Valores.Lugardenacimiento = valores[5];
                        Valores.Fechadenacimiento = valores[6];
                        Valores.Genero = valores[7];
                        Valores.Formacion = valores[8];

                        try
                        {
                            _context.Usuarios.Add(Valores);
                            await _context.SaveChangesAsync();
                        }
                        catch (Exception e)
                        {
                            e = null;
                        }


                        sb.AppendLine("</tr>");
                    }
                    sb.Append("</table>");
                }
            }
            return this.Content(sb.ToString());
        }

       
    }
}
