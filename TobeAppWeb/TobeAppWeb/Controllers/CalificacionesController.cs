﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TobeAppWeb.Models;

namespace TobeAppWeb.Controllers
{
    public class CalificacionesController : Controller
    {
        private readonly AppTobeContext _context;

        public CalificacionesController(AppTobeContext context)
        {
            _context = context;
        }

        // GET: Calificaciones
        public async Task<IActionResult> Index()
        {
            var appTobeContext = _context.Calificaciones.Include(c => c.IdItemsNavigation).Include(c => c.IdSedeNavigation).Include(c => c.IdUsuarioNavigation);
            return View(await appTobeContext.ToListAsync());
        }

        // GET: Calificaciones/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var calificaciones = await _context.Calificaciones
                .Include(c => c.IdItemsNavigation)
                .Include(c => c.IdSedeNavigation)
                .Include(c => c.IdUsuarioNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (calificaciones == null)
            {
                return NotFound();
            }

            return View(calificaciones);
        }

        // GET: Calificaciones/Create
        public IActionResult Create()
        {
            ViewData["IdItems"] = new SelectList(_context.Items, "Id", "NombreItem");
            ViewData["IdSede"] = new SelectList(_context.Sedes, "Id", "Ciudad");
            ViewData["IdUsuario"] = new SelectList(_context.Usuarios, "Id", "Contraseña");
            return View();
        }

        // POST: Calificaciones/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,IdUsuario,IdItems,IdSede,Fecha,Valoracion,Comentario")] Calificaciones calificaciones)
        {
            if (ModelState.IsValid)
            {
                _context.Add(calificaciones);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdItems"] = new SelectList(_context.Items, "Id", "NombreItem", calificaciones.IdItems);
            ViewData["IdSede"] = new SelectList(_context.Sedes, "Id", "Ciudad", calificaciones.IdSede);
            ViewData["IdUsuario"] = new SelectList(_context.Usuarios, "Id", "Contraseña", calificaciones.IdUsuario);
            return View(calificaciones);
        }

        // GET: Calificaciones/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var calificaciones = await _context.Calificaciones.FindAsync(id);
            if (calificaciones == null)
            {
                return NotFound();
            }
            ViewData["IdItems"] = new SelectList(_context.Items, "Id", "NombreItem", calificaciones.IdItems);
            ViewData["IdSede"] = new SelectList(_context.Sedes, "Id", "Ciudad", calificaciones.IdSede);
            ViewData["IdUsuario"] = new SelectList(_context.Usuarios, "Id", "Contraseña", calificaciones.IdUsuario);
            return View(calificaciones);
        }

        // POST: Calificaciones/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,IdUsuario,IdItems,IdSede,Fecha,Valoracion,Comentario")] Calificaciones calificaciones)
        {
            if (id != calificaciones.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(calificaciones);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CalificacionesExists(calificaciones.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["IdItems"] = new SelectList(_context.Items, "Id", "NombreItem", calificaciones.IdItems);
            ViewData["IdSede"] = new SelectList(_context.Sedes, "Id", "Ciudad", calificaciones.IdSede);
            ViewData["IdUsuario"] = new SelectList(_context.Usuarios, "Id", "Contraseña", calificaciones.IdUsuario);
            return View(calificaciones);
        }

        // GET: Calificaciones/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var calificaciones = await _context.Calificaciones
                .Include(c => c.IdItemsNavigation)
                .Include(c => c.IdSedeNavigation)
                .Include(c => c.IdUsuarioNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (calificaciones == null)
            {
                return NotFound();
            }

            return View(calificaciones);
        }

        // POST: Calificaciones/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var calificaciones = await _context.Calificaciones.FindAsync(id);
            _context.Calificaciones.Remove(calificaciones);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool CalificacionesExists(int id)
        {
            return _context.Calificaciones.Any(e => e.Id == id);
        }
    }
}
