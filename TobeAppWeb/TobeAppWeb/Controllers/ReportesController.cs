﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TobeAppWeb.Models;

namespace TobeAppWeb.Controllers
{
    public class ReportesController : Controller
    {
        private readonly AppTobeContext _context;

        public ReportesController(AppTobeContext context)
        {
            _context = context;
        }

        [Authorize(Roles = "SuperAdmin")]
        public IActionResult Index()
        {
            return View();
        }


        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> IndexAdminAsync()
        {
            var datosUsua = await _context.AspNetUsers.SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
            var appTobeContext = await _context.Entidades.SingleOrDefaultAsync(s => s.Id== datosUsua.IdEntidad);
            ViewData["link"] = appTobeContext.LinkReportes; 
            return View();
        }
    }
}
