﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TobeAppWeb.Modelos;
using TobeAppWeb.Models;
using System.Web;
using System.IO;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using System.Text.Encodings.Web;

namespace TobeAppWeb.Controllers
{
    [Authorize(Roles = "SuperAdmin")]
    public class Usuarios_Controller : Controller
    {
        private readonly AppTobeContext _context;
        public static IWebHostEnvironment _enviro;

        public Usuarios_Controller(AppTobeContext context, IWebHostEnvironment enviro)
        {
            _context = context;
            _enviro = enviro;
        }

        // GET: Usuarios
        public async Task<IActionResult> Index()
        {
            return View(await _context.Usuarios.ToListAsync());
        }

        [AllowAnonymous]
        public IActionResult RestaurarContraseña()
        {
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> RestaurarContraseña(RestaurarContraseña restaurar)
        {
            if (ModelState.IsValid)
            {
                var consulta = await _context.Usuarios.SingleOrDefaultAsync(s => s.Correo == restaurar.CorreoElectronico);

                if (consulta == null)
                {
                    TempData["Mensaje"] = "Correo no encontrado";
                    
                     return RedirectToAction("RestaurarContraseña"); 
                }
                else
                {
                    var codigo = Encrypt.GetSha256(restaurar.CorreoElectronico);
                    var nube = "http://tobeappweb-env.us-east-2.elasticbeanstalk.com";
                    // var local = "https://localhost:44320";
                    string link = nube + "/Usuarios_/CambiarContraseña/" + codigo;

                    var mensaje = $"Por favor entre al siguiente link para cambiar su contraseña <a href='{HtmlEncoder.Default.Encode(link)}'>click aquí</a>.";
                    EnviarCorreo.Enviar(restaurar.CorreoElectronico, "Cambio de contraseña", mensaje);
                    //return RedirectToAction("CambiarContraseña", new { Id = consulta.Id });
                    TempData["Mensaje"] = "Correo enviado";

                    return RedirectToAction("RestaurarContraseña");
                }
            }
            return View(restaurar);

        }

        [AllowAnonymous]
        public IActionResult CambiarContraseña(string? Id)
        {
            ViewBag.roleId = Id;
          //  ViewBag.Codi = codi;
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CambiarContraseña(CambiarContraseña restaurar, string Id)
        {
            var correo = Encrypt.GetSha256(restaurar.correo);
            if (correo == Id)
            {
                if (ModelState.IsValid)
                {
                    var consulta = await _context.Usuarios.SingleOrDefaultAsync(s => s.Correo == restaurar.correo);
                    consulta.Contraseña = Encrypt.GetSha256(restaurar.Contraseña);
                    var resul = _context.SaveChanges();

                    if (resul > 0)
                    {
                        return RedirectToAction("RestaurarContraseña");
                    }
                    else
                    {
                        TempData["Mensaje"] = "Error al guardar los datos";
                        return RedirectToAction("CambiarContraseña", new { Id = Id });
                    }
                }
            }
            else
            {
                TempData["Mensaje"] = "Correo invalido";
                return RedirectToAction("CambiarContraseña", new { Id = Id });
            }
            return View(restaurar);

        }


        // GET: Usuarios/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var usuarios = await _context.Usuarios
                .FirstOrDefaultAsync(m => m.Id == id);
            if (usuarios == null)
            {
                return NotFound();
            }

            return View(usuarios);
        }

        // GET: Usuarios/Create
        public IActionResult Create()
        {
            List<string> tipo = new List<string>();
            tipo.Add("No tiene formación");
            tipo.Add("Bachiller");
            tipo.Add("Profesional");
            tipo.Add("Magister");
            tipo.Add("Doctorado");
            tipo.Add("Posgrado");
            ViewData["Formacion"] = new SelectList(tipo);

            List<string> genero = new List<string>();
            genero.Add("Femenino");
            genero.Add("Masculino");
            ViewData["genero"] = new SelectList(genero);
            ViewData["Identidad"] = new SelectList(_context.Entidades, "Id", "Entidad");
            return View();
        }

        // POST: Usuarios/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nombre,Nombredeusuario,Contraseña,Permiso,Identidad,Correo,Telefono,Lugardenacimiento,Fechadenacimiento,Genero,Formacion")] Usuarios usuarios)
        {
            if (ModelState.IsValid)
            {
                var confirm = await _context.Usuarios.SingleOrDefaultAsync(s => s.Nombredeusuario == usuarios.Nombredeusuario || s.Correo == usuarios.Correo);
                var confirmcorre = await _context.Usuarios.SingleOrDefaultAsync(s => s.Correo == usuarios.Correo);
                if (confirm == null && confirmcorre== null)
                {
                    Usuarios Valores = new Usuarios();
                    Valores.Nombre = usuarios.Nombre;
                    Valores.Nombredeusuario = usuarios.Nombredeusuario;
                    Valores.Contraseña = Encrypt.GetSha256(usuarios.Contraseña);
                    Valores.Correo = usuarios.Correo;
                    Valores.Telefono = usuarios.Telefono;
                    Valores.Lugardenacimiento = usuarios.Lugardenacimiento;
                    Valores.Fechadenacimiento = usuarios.Fechadenacimiento;
                    Valores.Genero = usuarios.Genero;
                    Valores.Formacion = usuarios.Formacion;
                    var usua = _context.Add(Valores);
                    var res =await _context.SaveChangesAsync();
                    if (res > 0)
                    {
                              PermisoUsuarios nn = new PermisoUsuarios();
                              nn.IdUsuarios = Valores.Id;
                               nn.IdEntidad = 21;
                              _context.PermisoUsuarios.Add(nn);
                             await _context.SaveChangesAsync();
                    }
                        return RedirectToAction(nameof(Index));
                }
                else
                {
                    TempData["Mensaje"] = "El nombre usuario o correo ya esta registrado";
                    return RedirectToAction("Create");
                }
            }
           // ViewData["Identidad"] = new SelectList(_context.Entidades, "Id", "Entidad", usuarios.Identidad);
            return View(usuarios);
        }

        // GET: Usuarios/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var usuarios = await _context.Usuarios.FindAsync(id);
            if (usuarios == null)
            {
                return NotFound();
            }
            Usuarios Valores = new Usuarios();
            Valores.Id = usuarios.Id;
            Valores.Nombre = usuarios.Nombre;
            Valores.Nombredeusuario = usuarios.Nombredeusuario;
            Valores.Contraseña = usuarios.Contraseña;
            Valores.Correo = usuarios.Correo;
            Valores.Telefono = usuarios.Telefono;
            Valores.Lugardenacimiento = usuarios.Lugardenacimiento;
            Valores.Fechadenacimiento = usuarios.Fechadenacimiento;
            Valores.Genero = usuarios.Genero;
            Valores.Formacion = usuarios.Formacion;

            List<string> tipo = new List<string>();
            tipo.Add("No tiene formación");
            tipo.Add("Bachiller");
            tipo.Add("Profesional");
            tipo.Add("Magister");
            tipo.Add("Doctorado");
            tipo.Add("Posgrado");
            ViewData["Formacion"] = new SelectList(tipo);

            List<string> genero = new List<string>();
            genero.Add("Femenino");
            genero.Add("Masculino");
            ViewData["genero"] = new SelectList(genero);
            return View(Valores);
        }

        // POST: Usuarios/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nombre,Nombredeusuario,Contraseña,Permiso,Identidad,Correo,Telefono,Lugardenacimiento,Fechadenacimiento,Genero,Formacion")] Usuarios usuarios)
        {
            if (id != usuarios.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    try
                    {
                        _context.Update(usuarios);
                        await _context.SaveChangesAsync();
                    }
                        
                    catch (Exception e)
                     {
                    TempData["Mensaje"] = "El nombre de usuario o correo esta registrado";
                    return RedirectToAction("Index");
                     }


            }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UsuariosExists(usuarios.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
           // ViewData["Identidad"] = new SelectList(_context.Entidades, "Id", "Entidad", usuarios.Identidad);
            return View(usuarios);
        }

        // GET: Usuarios/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var usuarios = await _context.Usuarios
                .FirstOrDefaultAsync(m => m.Id == id);
            if (usuarios == null)
            {
                return NotFound();
            }

            return View(usuarios);
        }

        // POST: Usuarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var usuarios = await _context.Usuarios.FindAsync(id);
            _context.Usuarios.Remove(usuarios);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool UsuariosExists(int id)
        {
            return _context.Usuarios.Any(e => e.Id == id);
        }
    }
}
