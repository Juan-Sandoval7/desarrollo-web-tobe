﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using TobeAppWeb.Modelos;
using Microsoft.AspNetCore.Authorization;
using TobeAppWeb.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace TobeAppWeb.Controllers
{
    [Authorize(Roles = "SuperAdmin")]
    public class AdminisController : Controller
    {
        //private readonly RoleManager<IdentityRole> roleManager;
        private readonly RoleManager<IdentityRole> roleManager;
        private readonly UserManager<ApplicationUser> UsuarioManager;
        private readonly AppTobeContext _context;
        public AdminisController(RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> UsuarioManager, AppTobeContext context)
        {
            _context = context;
            this.roleManager = roleManager;
            this.UsuarioManager = UsuarioManager; 
        }

        [HttpGet]
        public IActionResult CreateRole()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> CreateRole(CrearRol model)
        {
            if (ModelState.IsValid)
            {
                // We just need to specify a unique role name to create a new role
                IdentityRole identityRole = new IdentityRole
                {
                    Name = model.NombreRol
                };

                // Saves the role in the underlying AspNetRoles table
                IdentityResult result = await roleManager.CreateAsync(identityRole);

                if (result.Succeeded)
                {
                    return RedirectToAction("ListRoles");
                }

                foreach (IdentityError error in result.Errors)
                {
                    ModelState.AddModelError("", error.Description);
                }
            }

            return View(model);
        }

        public ViewResult ListRoles() => View(roleManager.Roles);


        [HttpGet]
        public async Task<ActionResult> EditarRol (String id)
        {
            
            var rol = await roleManager.FindByIdAsync(id); 

            if (rol == null)
            {
                ViewBag.ErrorMessage = $"Rol con el id=  {id} no fue encontrado";
                return View("Error"); 
            }

            var model = new EditarRolViewmodel
            {
                Id = rol.Id,
                RolNombre = rol.Name
            };

            foreach (var usuario in UsuarioManager.Users)
            {
                if(await UsuarioManager.IsInRoleAsync(usuario, rol.Name))
                {
                    model.Usuarios.Add(usuario.UserName);
                }
            }

            return View(model); 

        }

        [HttpPost]
        public async Task<ActionResult> EditarRol(EditarRolViewmodel model)
        {
            var rol = await roleManager.FindByIdAsync(model.Id); 

            if (rol == null)
            {
                ViewBag.ErrorMessage = $"Rol con el id=  {model.Id} no fue encontrado";
                return View("Error");
            }
            else
            {
                rol.Name = model.RolNombre;
                var resultado = await roleManager.UpdateAsync(rol); 
                if (resultado.Succeeded)
                {
                    return RedirectToAction("ListRoles"); 
                }
                foreach (var error in resultado.Errors)
                {
                    ModelState.AddModelError("", error.Description); 
                }

                return View(model); 
            }
        }

        [HttpGet]
        public async Task<ActionResult> EditarUsuarioRol(String RolId)
        {
            ViewBag.roleId = RolId;

            var role = await roleManager.FindByIdAsync(RolId); 

            if (role == null)
            {
                ViewBag.ErrorMessage = $"Rol con el id=  {RolId} no fue encontrado";
                return View("Error");
            }

            var model = new List<UsuarioRolmodelo>();

            foreach (var usuario in UsuarioManager.Users)
            {

                var usuarioRolmodelo = new UsuarioRolmodelo
                {
                    UsuarioID = usuario.Id,
                    UsuarioNombre = usuario.UserName,
                }; 

                if (await UsuarioManager.IsInRoleAsync(usuario, role.Name))
                {
                    usuarioRolmodelo.EstaSeleccionado = true; 
                }
                else
                {
                    usuarioRolmodelo.EstaSeleccionado = false;
                }

                model.Add(usuarioRolmodelo); 
            }
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> EditarUsuarioRol(List<UsuarioRolmodelo> model, string RolId)
        {
            //var Datosu = RolId;
            var rol = await roleManager.FindByIdAsync(RolId);

              if(rol == null)
               {
                ViewBag.ErrorMessage = $"Rol con el id=  {RolId} no fue encontrado";
                 return View("Error");
              }

            for (int i= 0; i < model.Count; i++)
            {
                var user = await UsuarioManager.FindByIdAsync(model[i].UsuarioID);
                IdentityResult result = null; 

                if (model[i].EstaSeleccionado && !(await UsuarioManager.IsInRoleAsync(user, rol.Name)))
                {
                    result = await UsuarioManager.AddToRoleAsync(user, rol.Name); 
                }
                else if (!model[i].EstaSeleccionado && (await UsuarioManager.IsInRoleAsync(user, rol.Name)))
                {
                    result = await UsuarioManager.RemoveFromRoleAsync(user, rol.Name);
                }
                else
                {
                    continue;
                }

                if (result.Succeeded)
                {
                    if (i < model.Count - 1)
                    {
                        continue; 
                    }
                    else
                    {
                        return RedirectToAction("EditarRol", new { Id = RolId });
                    }
                }

            }
            return RedirectToAction("EditarRol", new { Id = RolId });


        }

        [HttpGet]
        public IActionResult RegistroUsuarioAdmin()
        {
            ViewData["permiso"] = 2; 
            ViewData["IdEntidad"] = new SelectList(_context.Entidades, "Id", "Entidad");
            return View();
        }




    }
}
