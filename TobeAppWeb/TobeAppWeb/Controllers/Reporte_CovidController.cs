﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using TobeAppWeb.Modelos;
using TobeAppWeb.Models;

namespace TobeAppWeb.Controllers
{
    [Authorize(Roles = "Admin")]
    public class Reporte_CovidController : Controller
    {
        private readonly AppTobeContext _context;

        public Reporte_CovidController(AppTobeContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> IndexEnviarNotifi()
        {
             int año = DateTime.Today.Year;
             int dia = DateTime.Today.Day;
             int mes = DateTime.Today.Month;
            var datosUsua = await _context.AspNetUsers.SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
            var appTobeContext = _context.ReporteCovid.Include(r => r.IdUsuarioNavigation).Include(r => r.IdSedesNavigation.IdEntidadNavigation).Where(s => s.IdSedesNavigation.IdEntidad == datosUsua.IdEntidad && s.Notificado == 0 && s.NumerodeSintomas >= 3);
            DateTime fecha = new DateTime(año,mes,dia);
            var model = new List<ReporteCovidModel>();

            foreach (var info in appTobeContext)
            {

                var ReporteCovidModel = new ReporteCovidModel
                {
                    IdUsuario = info.IdUsuario,
                    Nombredeusuario = info.IdUsuarioNavigation.Nombredeusuario,
                    IdReporteCovid = info.Id,
                    Sede = info.IdSedesNavigation.NombreSede,
                    EntidadNotifica = info.IdSedesNavigation.IdEntidadNavigation.Entidad,
                    FechadeReporte = (DateTime)info.Fecha,
                    FechaActualparaNotificar = fecha,
                    NumerodeSintomas = info.NumerodeSintomas,
                    IraTrabajar = true,
                    CitaTelemedicina = false,
                    EstaSeleccionado = false,

                };

                

                model.Add(ReporteCovidModel);
            }
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> IndexEnviarNotifi(List<ReporteCovidModel> model)
        {
            

            for (int i = 0; i < model.Count; i++)
            {
                int result = 0;

                if (model[i].EstaSeleccionado)
                {
                    string iratr = null;
                    string citate = null;
                    if (model[i].IraTrabajar)
                    {
                         iratr = "No";
                    }
                    else
                    {
                         iratr = "Ir";
                    }
                    if (model[i].CitaTelemedicina)
                    {
                        citate = "Si";
                    }
                    else
                    {
                        citate = "No";
                    }
                    NotificacionesaReporteCovid noti = new NotificacionesaReporteCovid
                    {
                        //IdUsuario = model[i].IdUsuario,
                        EntidadNotifica = model[i].EntidadNotifica,
                        IdReporteCovid = model[i].IdReporteCovid,
                        FechaNotifica = model[i].FechaActualparaNotificar,
                        IraTrabajar = iratr,
                        CitaTelemedicina = citate,

                    }; 
                    _context.NotificacionesaReporteCovid.Add(noti);
                    result =await _context.SaveChangesAsync();
                    var ReporteCovid = await _context.ReporteCovid.FindAsync(model[i].IdReporteCovid);
                    ReporteCovid.Notificado = 1;
                    _context.SaveChanges();
                }
                else
                {
                    continue;
                }

                if (result>0)
                {
                    if (i < model.Count - 1)
                    {
                        continue;
                    }
                    else
                    {
                        return RedirectToAction("IndexEnviarNotifi");
                    }
                }

            }
            return RedirectToAction("IndexEnviarNotifi");


        }

        // GET: ReporteCovids
        public async Task<IActionResult> Index()
        {
            var datosUsua = await _context.AspNetUsers.SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
            var appTobeContext = _context.ReporteCovid.Include(s => s.IdUsuarioNavigation).Include(s => s.IdSedesNavigation).Where(s => s.IdSedesNavigation.IdEntidad == datosUsua.IdEntidad && s.Notificado == 1);
            return View(await appTobeContext.ToListAsync());
        }

        public async Task<IActionResult> IndexSinNotifi()
        {
            var datosUsua = await _context.AspNetUsers.SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
            var appTobeContext = _context.ReporteCovid.Include(s => s.IdUsuarioNavigation).Include(s => s.IdSedesNavigation).Where(s => s.IdSedesNavigation.IdEntidad == datosUsua.IdEntidad && s.Notificado == 0);
            return View(await appTobeContext.ToListAsync());
        }

        // GET: ReporteCovids/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var reporteCovid = await _context.ReporteCovid
                .Include(r => r.IdSedesNavigation)
                .Include(r => r.IdUsuarioNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (reporteCovid == null)
            {
                return NotFound();
            }
            var datosUsua = await _context.AspNetUsers.SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
            if (reporteCovid.IdSedesNavigation.IdEntidad != datosUsua.IdEntidad)
            {
                return NotFound();
            }

            return View(reporteCovid);
        }

      
        // GET: ReporteCovids/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var reporteCovid = await _context.ReporteCovid
                .Include(r => r.IdSedesNavigation)
                .Include(r => r.IdUsuarioNavigation)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (reporteCovid == null)
            {
                return NotFound();
            }
            var datosUsua = await _context.AspNetUsers.SingleOrDefaultAsync(s => s.UserName == User.Identity.Name);
            if (reporteCovid.IdSedesNavigation.IdEntidad != datosUsua.IdEntidad)
            {
                return NotFound();
            }
            return View(reporteCovid);
        }

        // POST: ReporteCovids/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var reporteCovid = await _context.ReporteCovid.FindAsync(id);
            _context.ReporteCovid.Remove(reporteCovid);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ReporteCovidExists(int id)
        {
            return _context.ReporteCovid.Any(e => e.Id == id);
        }
    }
}
