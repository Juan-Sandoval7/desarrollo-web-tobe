﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TobeAppWeb.Modelos
{
    public class CrearRol
    {
        [Required(ErrorMessage = "El nombre del rol es obligatorio")]
        public string NombreRol { get; set; }
    }
}
