﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TobeAppWeb.Modelos
{
    public class UsuarioRolmodelo
    {
        public string UsuarioID { get; set; }
        public string UsuarioNombre { get; set; }
        public bool EstaSeleccionado { get; set; }

    }
}
