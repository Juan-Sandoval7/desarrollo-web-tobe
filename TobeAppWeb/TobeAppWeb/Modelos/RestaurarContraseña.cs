﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TobeAppWeb.Modelos
{
    public class RestaurarContraseña
    {
        [Required(ErrorMessage = "Correo no encontrado")]
        [EmailAddress(ErrorMessage = "Correo no valido")]
        public string CorreoElectronico { get; set; }

     
    }
}
