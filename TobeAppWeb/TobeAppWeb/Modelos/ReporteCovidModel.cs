﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TobeAppWeb.Models;

namespace TobeAppWeb.Modelos
{
    public class ReporteCovidModel
    {
        [Required]
        public int? IdUsuario { get; set; }
        [Required]
        public string Nombredeusuario { get; set; }
        [Required]
        public int? IdReporteCovid { get; set; }
        [Required]
        public string EntidadNotifica { get; set; }
        [Required]
        public string Sede { get; set; }
        [Required]
        public DateTime FechaActualparaNotificar { get; set; }
        [Required]
        public DateTime FechadeReporte { get; set; }
        [Required]
        public int NumerodeSintomas { get; set; }
        [Required]
        public bool IraTrabajar { get; set; }
        [Required]
        public bool CitaTelemedicina { get; set; }

        public bool EstaSeleccionado { get; set; }


    }
}
