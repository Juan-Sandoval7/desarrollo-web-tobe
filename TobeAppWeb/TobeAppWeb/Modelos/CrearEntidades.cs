﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TobeAppWeb.Modelos
{
    public class CrearEntidades
    {

        public int Id { get; set; }

        [Required]
        public string Entidad { get; set; }
        [Required]
        public string Descripcion { get; set; }

        [Required]
        [Display(Name = "File")]
        public IFormFile Logo { get; set; }
        [Required]
        public string Color { get; set; }

        public string LinkReportes { get; set; }
    }

}
