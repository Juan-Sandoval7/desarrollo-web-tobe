﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TobeAppWeb.Modelos
{
    public class CambiarContraseña
    {

        // [Required]
        // public int Id { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Correo no valido")]
        public string correo { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Contraseña { get; set; }

       // [Required]
       // [DataType(DataType.Password)]
       // [Display(Name = "Confirm password")]
       // [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
       // public string ConfirmarContraseña { get; set; }
    }
}
