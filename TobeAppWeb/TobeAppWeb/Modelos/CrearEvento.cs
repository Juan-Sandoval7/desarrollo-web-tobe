﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TobeAppWeb.Modelos
{
    public class CrearEvento
    {


        [Required]
        public int IdSede { get; set; }

        [Required]
        public int Tipo { get; set; }

        [Required]
        public string Titulo { get; set; }

        [Required]
        public string Descripcion { get; set; }

        [Required]
        [Display(Name = "File")]
        public IFormFile Imagen { get; set; }
    }
}
