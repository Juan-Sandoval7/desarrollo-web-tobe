﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TobeAppWeb.Modelos
{
    public class RegistroUsuariosAdmin
    {
        [Required]
        [EmailAddress]
        public string correo { get; set;  }


        [Required]
        [DataType(DataType.Password)]
        public string password { get; set; }


        [Required]
        [Display(Name= "Repetir Contraseña")]
        [Compare("password",ErrorMessage = "La contraseña no coincide")]
        public string passwordValidar { get; set; }

        [Required]
        public int Permiso { get; set; }

        [Required]
        public int IdEntidad { get; set; }

    }
}
