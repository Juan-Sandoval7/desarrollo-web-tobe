﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace TobeAppWeb.Modelos
{
    public class ApplicationUser : IdentityUser

    {
        [Required(ErrorMessage = "El permiso es obligatorio para acceder a la aplicación movil")]
        public int Permiso { get; set;}

        [Required(ErrorMessage = "La identidad es obligatoria")]
        public int IdEntidad { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(Microsoft.AspNetCore.Identity.UserManager<ApplicationUser> manager)
        {
            var authenticationType = DefaultAuthenticationTypes.ApplicationCookie;
            var userIdentity = new ClaimsIdentity(await manager.GetClaimsAsync(this), authenticationType);

            // Add custom user claims here
            return userIdentity;
        }

    }
}
