﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TobeAppWeb.Modelos
{
    public class EditarRolViewmodel
    {
        public string Id { get; set; }

        public EditarRolViewmodel()
        {
            Usuarios = new List<String>();
        }

        [Required(ErrorMessage ="El nombre del rol es obligatorio")]

        public string RolNombre { get; set; }

        public List<string> Usuarios { get; set; }


    }
}
